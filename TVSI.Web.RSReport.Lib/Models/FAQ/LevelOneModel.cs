﻿namespace TVSI.Web.RSReport.Lib.Models.FAQ
{
    public class LevelOneModel
    {
        
        public string NameGroup {get;set;}
        public string Id { get; set; }
        public string KeySearch { get; set; } = "";
        public int Page { get; set; } = 0;
    }
}