﻿using System;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.Lib.Models.FAQ
{
    public class LevelTwoModel
    {
        public int Id { get; set; }
        public string NameGroupB { get; set; }
        public string NameGroupA { get; set; }
        public string KeySearch { get; set; } = "";
        public int Page { get; set; } = 0;

        public string CreateBy { get; set; }

        public DateTime CreateDate { get; set; }
    }

    public class LevelTwoGrModel
    {
        public int Id { get; set; }
        public int Id_A { get; set; }
        public string GroupName { get; set; }
    }
}