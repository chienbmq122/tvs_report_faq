﻿using System;

namespace TVSI.Web.RSReport.Lib.Models.FAQ
{
    public class FAQFavoritesModel
    {
        public int id { get; set; }
        public int id_C { get; set; }
        
    }

    public class FAQFavoriteslistModel
    {
        public int id { get; set; }
        public int id_C { get; set; }
        public int id_B { get; set; }
        public int id_A { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public int FileID { get; set; }
        public string Hashtag { get; set; }
        public string GroupName_B { get; set; }
        public string GroupName_A { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string KeySearch { get; set; } = "";
        public int Page { get; set; }
    }
}