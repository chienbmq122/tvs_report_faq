﻿using System;
using System.Collections.Generic;
using System.Web;

namespace TVSI.Web.RSReport.Lib.Models.FAQ
{
    public class FAQQuestionLevelsModel
    {
        public string KeySearch { get; set; } = "";
        public int Page { get; set; } = 0;
    }

    public class TotalQuestionLevelsModel
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }

    public class FAQLevelOneModel
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
    }
    public class FAQLevelTwoModel
    {
        public int Id { get; set; }
        public int Id_A { get; set; }
        public string GroupName { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Hashtag{get;set;}
        
        public HttpPostedFileBase File { get; set; }
    }

    public class FAQLVTwoModel
    {
        public int Id { get; set; }
        public int Id_B { get; set; }
    }

    public class FAQFileName
    {
        public int id { get; set; }
        public string FileName { get; set; }
    }

    public class FAQModel
    {
        public int id { get; set; }
        public int Id_B { get; set; }
        public string QuestionName { get; set; }
        public string Answer { get; set; }
        
        public string Hashtag { get; set; }
        
        public int FileID { get; set; }
        public string CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string GroupNameB { get; set; }
        public string GroupNameA { get; set; }
        public string KeySearch { get; set; } = "";
        public int Page { get; set; } = 0;
        public HttpPostedFileBase File { get; set; }
        
    }
    
}