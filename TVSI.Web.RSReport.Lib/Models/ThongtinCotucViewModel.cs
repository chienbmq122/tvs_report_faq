﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVSI.Web.RSReport.Lib.Models
{
    public class ThongtinCotucViewModel
    {
        public string Ticker { get; set; } // Mã chứng khoán
        public DateTime DeclarationDate { get; set; } // Ngày thông  báo
        public DateTime ExDividendDate { get; set; } // Ngày giao dịch không hưởng quyền
        public DateTime LastRegDate { get; set; } // Ngày đăng ký cuối cùng
        public DateTime PaymentDate { get; set; } //Ngày thanh toán
        public decimal Value { get; set; } // Giá trị
        public decimal Rate { get; set; } // Tỷ lệ trả cổ tức
        public int CashYear { get; set; } // Năm trả cổ tức
        public string CashWave { get; set; } // Đợt trả cổ tức
        public string AddInfo { get; set; } // Đợt trả cổ tức
    }
}
