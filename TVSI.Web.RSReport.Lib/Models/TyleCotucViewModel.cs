﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVSI.Web.RSReport.Lib.Models
{
    public class TyleCotucViewModel
    {

        public string Ticker { get; set; }
        public decimal Tyletracotuc { get; set; }
        public decimal Cotuccuanam { get; set; }
        public decimal YearReport { get; set; }
        public string Note { get; set; }
    }
}
