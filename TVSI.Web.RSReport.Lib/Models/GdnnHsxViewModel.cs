﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVSI.Web.RSReport.Lib.Models
{
    public class GdnnHsxViewModel
    {
        public string Code { get; set; }
        public decimal KLMua { get; set; }
        public decimal GTMua { get; set; }
        public decimal KLBan { get; set; }
        public decimal GTBan { get; set; }
        public decimal KLMuaRong { get; set; }
        public decimal GTMuaRong { get; set; }
    }
}
