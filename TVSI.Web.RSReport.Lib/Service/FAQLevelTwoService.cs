﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;

namespace TVSI.Web.RSReport.Lib.Service
{
    public class FAQLevelTwoService : IFAQLevelTwoService
    {
        public async Task<bool> FAQDeleteLevelTwo(int id)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "UPDATE TVSI_FAQ_LEVEL_TWO SET status = 99 WHERE faq_two_id = @id";
                    return conn.Execute(sql,
                        new {@id = id}
                    ) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQLevelTwoService), "FAQDeleteLevelTwo - " +  e.Message);
                Logger.Debug(typeof(FAQLevelTwoService),"FAQDeleteLevelTwo - " +  e.InnerException);
                throw;
            }
        }

        public async Task<bool> FAQUpdateLevelTwo(int id, string groupname)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = " update TVSI_FAQ_LEVEL_TWO set group_name = @groupname where faq_two_id = @id";
                    return conn.Execute(sql,
                        new
                        {
                            @id = id,
                            @groupname = groupname
                        }
                    ) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQLevelTwoService), "FAQUpdateLevelTwo - " +  e.Message);
                Logger.Debug(typeof(FAQLevelTwoService),"FAQUpdateLevelTwo - " +  e.InnerException);
                throw;
            }
        }
    }
}