﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;
using TVSI.Web.RSReport.Lib.Models.FAQ;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.Lib.Service
{
    public class FAQLevelOneService : IFAQLevelOneService
    {
        public async Task<IEnumerable<LevelTwoModel>> GetListFAQLevelTwo(string keysearch)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "select a.faq_two_id Id, b.group_name NameGroupB, a.group_name NameGroupA,a.createby CreateBy,a.createdate CreateDate from TVSI_FAQ_LEVEL_TWO a inner join TVSI_FAQ_LEVEL_ONE b ON a.faq_one_id = b.faq_one_id where a.status = 1 and ( a.group_name LIKE @keysearch or b.group_name like @keysearch ) order by faq_two_id desc";

                    return conn.Query<LevelTwoModel>(sql, new
                    {
                        @keysearch = "%" + keysearch + "%"
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQLevelOneService), "GetListFAQLevelTwo - " +  e.Message);
                Logger.Debug(typeof(FAQLevelOneService),"GetListFAQLevelTwo - " +  e.InnerException);
                throw;
            }
        }
    }
}