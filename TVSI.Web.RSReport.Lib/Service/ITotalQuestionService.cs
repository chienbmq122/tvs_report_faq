﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public interface ITotalQuestionService
    {
        Task<IEnumerable<FAQLevelOneModel>> FAQLevelOneList();
        Task<IEnumerable<FAQLevelTwoModel>> FAQLevelTwoList();
        Task<IEnumerable<FAQLVTwoModel>> FAQGetListData();
        Task<IEnumerable<FAQModel>> FAQDetailLevelTwoList(int id);

        Task<IEnumerable<FAQModel>> FAQKeySearch(int id, string keysearch);
        Task<string> GetUserInfo(string userDomain);
        Task<string> GetGroupNameLvOne(int id);





    }
}