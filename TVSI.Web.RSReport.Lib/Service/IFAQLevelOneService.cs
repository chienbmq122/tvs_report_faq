﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public interface IFAQLevelOneService
    {
        Task<IEnumerable<LevelTwoModel>> GetListFAQLevelTwo(string keysearch);
    }
}