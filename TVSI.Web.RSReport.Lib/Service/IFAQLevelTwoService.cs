﻿using System.Threading.Tasks;

namespace TVSI.Web.RSReport.Lib.Service
{
    public interface IFAQLevelTwoService
    {
        Task<bool> FAQDeleteLevelTwo(int id);
        Task<bool> FAQUpdateLevelTwo(int id, string groupname);
    }
}