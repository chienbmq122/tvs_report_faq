﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public class FAQService : IFAQService
    {
        public async Task<bool> SaveQuestionsAndAnswers(FAQLevelTwoModel model,string createby)
        {
            try
            {
                
                string folderPath = ConfigurationManager.AppSettings["FOLDER_UPLOAD"].ToString();

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(folderPath))){
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(folderPath));
                }
                    
                int? fileId = null;
                
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    
                    if (model.File != null)
                    {
                        if (model.File.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(Guid.NewGuid().ToString("n").Substring(0,10) + "_" + model.File.FileName).Replace(" ","");
                            var fileLocal = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(folderPath), fileName);
                            model.File.SaveAs(fileLocal);
                            var param = new DynamicParameters();
                            param.Add("@file_id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            param.Add("@file_name",fileName);
                            param.Add("@createby",createby);
                            conn.Execute("TVSI_sINSERT_ATTACH_FILE", param ,commandType: CommandType.StoredProcedure);
                            fileId = param.Get<int>("@file_id");   
                        }

                    }

                    //luu cau hoi va tra loi
                    return  conn.Execute("TVSI_sINSERT_QUESTIONS_AND_ANSWERS", new
                    {
                        @id_b  = model.Id,
                        @question_name = model.Question,
                        @answer = model.Answer,
                        @hashtag = model.Hashtag,
                        @file_id = fileId,
                        @createby = createby
                    },commandType: CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQService), "FAQService - " +  e.Message);
                Logger.Debug(typeof(FAQService),"FAQService - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQModel>> GetListFAQ(string keysearch)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "select c.group_name GroupNameA,a.hashtag Hashtag,a.file_id FileID,b.group_name GroupNameB,a.faq_id id,a.faq_two_id id_b, a.question_name QuestionName, a.answer Answer, a.createby CreateBy,a.createdate CreateDate from TVSI_FAQ_QUESTIONS_AND_ANSWERS a inner join  TVSI_FAQ_LEVEL_TWO b ON a.faq_two_id = b.faq_two_id inner join TVSI_FAQ_LEVEL_ONE c ON b.faq_one_id = c.faq_one_id where ( a.question_name like @keysearch or a.answer like @keysearch or (RTRIM(a.hashtag) + ',') like @keysearch) and  a.status = 1";
                    return conn.Query<FAQModel>(sql,
                        new
                        {
                            @keysearch = "%" + keysearch +  "%"
                        });
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQService), "FAQService - " +  e.Message);
                Logger.Debug(typeof(FAQService),"FAQService - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQFileName>> GetFileName()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT file_id id, file_name FileName FROM TVSI_FAQ_ATTACH_FILE where status = 1";
                    return conn.Query<FAQFileName>(sql);
                }
            }
            catch (Exception e)
            { 
                Logger.Debug(typeof(TotalQuestionService), "GetFileName - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"GetFileName - " +  e.InnerException);
                throw;
            }
        }

        public async Task<bool> UpdateQuestionAndAnswer(int id, string question,string answer, int idb,HttpPostedFileBase file,int fileid,string createby,string hashtag)
        {
            try
            {
                string folderPath = ConfigurationManager.AppSettings["FOLDER_UPLOAD"].ToString();

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(folderPath))){
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(folderPath));
                }
                int? fileidsave = null;
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    if (fileid != null && fileid > 0)
                    {
                        if (file != null)
                        {
                            if (file.ContentLength > 0)
                            {
                                var fileName = Path
                                    .GetFileName(Guid.NewGuid().ToString("n").Substring(0, 10) + "_" + file.FileName)
                                    .Replace(" ", "");
                                var fileLocal = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(folderPath),
                                    fileName);
                                file.SaveAs(fileLocal);
                                var sqlud =
                                    "  update TVSI_FAQ_ATTACH_FILE set file_name = @fileName ,updateby = @createby , updateday = @update where file_id =  @fileid ";
                                conn.Execute(sqlud, new
                                {
                                    @fileName = fileName,
                                    @createby = createby,
                                    @update = DateTime.Now,
                                    @fileid = fileid
                                });
                            }
                        }
                    }
                    else
                    {
                        if (file != null)
                        {
                            if (file.ContentLength > 0)
                            {
                                var fileName = Path
                                    .GetFileName(Guid.NewGuid().ToString("n").Substring(0, 10) + "_" + file.FileName)
                                    .Replace(" ", "");
                                var fileLocal = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(folderPath),
                                    fileName);
                                file.SaveAs(fileLocal);
                                var param = new DynamicParameters();
                                param.Add("@file_id", dbType: DbType.Int32, direction: ParameterDirection.Output);
                                param.Add("@file_name", fileName);
                                param.Add("@createby", createby);
                                conn.Execute("TVSI_sINSERT_ATTACH_FILE", param,
                                    commandType: CommandType.StoredProcedure);
                                fileidsave = param.Get<int>("@file_id");
                            }
                        }
                    }

                    var sql = "update TVSI_FAQ_QUESTIONS_AND_ANSWERS set faq_two_id = @idb,question_name = @question, file_id = @fileid ,answer = @answer,hashtag = @hashtag where faq_id = @id ";
                    return conn.Execute(sql, new
                    {
                        @id = id,
                        @idb = idb,
                        @question = question,
                        @answer = answer,
                        @hashtag = hashtag,
                        @fileid = fileidsave ?? fileid
                    }) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQService), "UpdateQuestionAndAnswer - " +  e.Message);
                Logger.Debug(typeof(FAQService),"UpdateQuestionAndAnswer - " +  e.InnerException);
                throw;
            }
        }

        public async Task<bool> DeleteFAQ(int id)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "  UPDATE TVSI_FAQ_QUESTIONS_AND_ANSWERS SET status = 99 WHERE faq_id = @id";
                    return conn.Execute(sql, new
                    {
                        @id = id
                    }) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQService), "DeleteFAQ - " +  e.Message);
                Logger.Debug(typeof(FAQService),"DeleteFAQ - " +  e.InnerException);
                throw;
            }
        }
    }
}