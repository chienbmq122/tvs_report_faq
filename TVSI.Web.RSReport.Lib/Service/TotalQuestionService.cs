﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public class TotalQuestionService : ITotalQuestionService
    {
        public async Task<IEnumerable<FAQLevelOneModel>> FAQLevelOneList()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT faq_one_id Id,group_name GroupName FROM TVSI_FAQ_LEVEL_ONE WHERE status = 1";
                    return conn.Query<FAQLevelOneModel>(sql);
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "FAQList - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"FAQList - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQLevelTwoModel>> FAQLevelTwoList()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT faq_two_id Id,faq_one_id Id_A,group_name GroupName FROM TVSI_FAQ_LEVEL_TWO WHERE status = 1";
                    return conn.Query<FAQLevelTwoModel>(sql);
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "FAQList - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"FAQList - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQLVTwoModel>> FAQGetListData()
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT faq_id id,faq_two_id id_b  FROM TVSI_FAQ_QUESTIONS_AND_ANSWERS WHERE status = 1";
                    return conn.Query<FAQLVTwoModel>(sql);
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "FAQList - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"FAQList - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQModel>> FAQDetailLevelTwoList(int id)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT a.hashtag Hashtag, a.file_id FileID, a.faq_id Id,b.group_name GroupNameB,c.group_name GroupNameA , a.faq_two_id Id_B, a.question_name QuestionName, a.answer Answer, a.createby CreateBy,a.createdate  CreateDate  FROM TVSI_FAQ_QUESTIONS_AND_ANSWERS a  inner join TVSI_FAQ_LEVEL_TWO b ON a.faq_two_id = b.faq_two_id inner join TVSI_FAQ_LEVEL_ONE C ON b.faq_one_id = c.faq_one_id Where a.faq_two_id = @id and a.status = 1";
                    return conn.Query<FAQModel>(sql,new
                    {
                        @id = id
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "FAQList - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"FAQList - " +  e.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<FAQModel>> FAQKeySearch(int id, string keysearch)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "SELECT a.hashtag Hashtag, a.file_id FileID ,a.faq_id Id,b.group_name GroupNameB,c.group_name GroupNameA , a.faq_two_id Id_B, a.question_name QuestionName, a.answer Answer, a.createby CreateBy,a.createdate  CreateDate  FROM TVSI_FAQ_QUESTIONS_AND_ANSWERS a  inner join TVSI_FAQ_LEVEL_TWO b ON a.faq_two_id = b.faq_two_id inner join TVSI_FAQ_LEVEL_ONE C ON b.faq_one_id = c.faq_one_id Where {condition} a.status = 1 and (a.question_name like @KeySearch or a.answer like @KeySearch or (RTRIM(a.hashtag) + ',') LIKE '%' + @KeySearch + ',%')";
                    if (id == 0 || id == null)
                        sql  = sql.Replace("{condition}", " ");
                    else
                        sql = sql.Replace("{condition}", "a.faq_two_id = @id and");
                    
                    return conn.Query<FAQModel>(sql,new
                    {
                        @id = id,
                        @KeySearch = "%" + keysearch + "%"
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "FAQKeySearch - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"FAQKeySearch - " +  e.InnerException);
                throw;
            }
        }

        public async Task<string> GetUserInfo(string userDomain)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var sql = "select ho_ten from TVSI_THONG_TIN_TRUY_CAP_CHI_TIET where id_he_thong = @UserDomain";
                    return conn.QueryFirstOrDefault<string>(sql, new
                    {
                        @UserDomain = userDomain
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "GetUserInfo - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"GetUserInfo - " +  e.InnerException);
                throw;
            }
        }

        public async Task<string> GetGroupNameLvOne(int id)
        {
            try
            {
                var grName = string.Empty;
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    SqlCommand command = null;
                    SqlDataReader drd;
                    command =
                        new SqlCommand(
                            "select b.group_name from TVSI_FAQ_LEVEL_TWO a inner join TVSI_FAQ_LEVEL_ONE b on a.faq_one_id = b.faq_one_id where a.faq_two_id = @id and a.status = 1",conn);
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add("@id", id);
                    command.Connection.Open();
                    drd = command.ExecuteReader();
                    if (drd.Read())
                    {
                        grName = drd["group_name"].ToString();
                    } 
                }

                return grName;
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(TotalQuestionService), "GetGroupNameLvOne - " +  e.Message);
                Logger.Debug(typeof(TotalQuestionService),"GetGroupNameLvOne - " +  e.InnerException);
                throw;
            }
        }
    }
}