﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public class FAQFavoritesService : IFAQFavoritesService
    {
       public async Task<bool> AddFavorite(int id,string createby)
        {
            try
            {
                using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    var param = new DynamicParameters();
                    param.Add("@id",id);
                    param.Add("@createby",createby);
                    return conn.Execute("TVSI_sINSERT_FAVORITES_QUESTIONS", param,
                        commandType: CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(FAQFavoritesService), "AddFavorite - " +  e.Message);
                Logger.Debug(typeof(FAQFavoritesService),"AddFavorite - " +  e.InnerException);
                throw;
            }
        }

       public async Task<IEnumerable<FAQFavoritesModel>> GetFavoritelist(string username)
       {
           try
           {
               using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
               {
                   var sql =
                       " SELECT favo_id id, faq_id id_C FROM TVSI_FAQ_FAVORITES_QUESTIONS WHERE createby = @username AND status = 1";
                   return conn.Query<FAQFavoritesModel>(sql, new
                   {
                       @username = username
                   });
               }
               
           }
           catch (Exception e)
           {
               Logger.Debug(typeof(FAQFavoritesService), "GetFavoritelist - " +  e.Message);
               Logger.Debug(typeof(FAQFavoritesService),"GetFavoritelist - " +  e.InnerException);
               throw;
           }
       }

       public async Task<bool> RemoveFavorite(int id,string username)
       {
           try
           {
               using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
               {
                   var sql =
                       " UPDATE TVSI_FAQ_FAVORITES_QUESTIONS SET status = 99,updateby = @createby,updateday = GETDATE() WHERE faq_id = @id_C";
                   return conn.Execute(sql, new
                   {
                       @id_C = id,
                       @createby = username
                   }) > 0;
               }
           }
           catch (Exception e)
           {
               Logger.Debug(typeof(FAQFavoritesService), "RemoveFavorite - " +  e.Message);
               Logger.Debug(typeof(FAQFavoritesService),"RemoveFavorite - " +  e.InnerException);
               throw;
           }
       }

       public async Task<IEnumerable<FAQFavoriteslistModel>> GetFavoriteslistall(string keysearch,string username)
       {
           try
           {
               using (var conn = new SqlConnection(ConnectionFactory.EmsConnectionString))
               {
                   var sql =
                       "select d.favo_id id,c.createby CreateBy,c.createdate CreateDate, c.faq_id id_C, b.faq_two_id id_B,a.faq_one_id id_A,c.question_name Question, c.answer Answer,c.file_id FileID, c.hashtag Hashtag, b.group_name GroupName_B, a.group_name GroupName_A from TVSI_FAQ_FAVORITES_QUESTIONS d inner join TVSI_FAQ_QUESTIONS_AND_ANSWERS c on d.faq_id = c.faq_id inner join TVSI_FAQ_LEVEL_TWO b on c.faq_two_id = b.faq_two_id inner join TVSI_FAQ_LEVEL_ONE a on b.faq_one_id = a.faq_one_id where d.status = 1 and d.createby = @username and  ( c.question_name like @keysearch or c.answer like @keysearch or b.group_name like @keysearch or a.group_name like @keysearch )";
                   return conn.Query<FAQFavoriteslistModel>(sql, new
                   {
                       @username = username,
                       @keysearch = "%" + keysearch + "%"
                   });
               }
           }
           catch (Exception e)
           {
               Logger.Debug(typeof(FAQFavoritesService), "GetFavoriteslistall - " +  e.Message);
               Logger.Debug(typeof(FAQFavoritesService),"GetFavoriteslistall - " +  e.InnerException);
               throw;
           }
       }
    }
}