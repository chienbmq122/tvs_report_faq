﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public interface IFAQFavoritesService
    {
        Task<bool> AddFavorite(int id,string createby);
        Task<IEnumerable<FAQFavoritesModel>> GetFavoritelist(string username);
        Task<bool> RemoveFavorite(int id,string username);
        
        Task<IEnumerable<FAQFavoriteslistModel>> GetFavoriteslistall(string keysearch ,string username);
    }
}