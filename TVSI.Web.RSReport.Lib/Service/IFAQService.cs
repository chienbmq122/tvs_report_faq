﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Service
{
    public interface IFAQService
    {
        Task<bool> SaveQuestionsAndAnswers(FAQLevelTwoModel model,string createby);
        Task<IEnumerable<FAQModel>> GetListFAQ(string keysearch);
        Task<IEnumerable<FAQFileName>> GetFileName();
        Task<bool> UpdateQuestionAndAnswer(int id,string question,string answer, int idb,HttpPostedFileBase file,int fileid,string createby,string hashtag);
        Task<bool> DeleteFAQ(int id);
    }
}