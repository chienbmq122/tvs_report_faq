﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using Dapper;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Infrastructure;
using TVSI.Web.RSReport.Lib.Models.FAQ;

namespace TVSI.Web.RSReport.Lib.Utility
{
    public class RPTUtils
    {
        public static string RemoveInvalidXmlChars(string text)
        {
            if (text == null)
                return text;
            if (text.Length == 0)
                return text;

            // a bit complicated, but avoids memory usage if not necessary
            StringBuilder result = null;
            for (int i = 0; i < text.Length; i++)
            {
                var ch = text[i];
                if (XmlConvert.IsXmlChar(ch))
                {
                    result.Append(ch);
                }
                else if (result == null)
                {
                    result = new StringBuilder();
                    result.Append(text.Substring(0, i));
                }
            }

            if (result == null)
                return text; // no invalid xml chars detected - return original text
            else
                return result.ToString();

        }

        public static string UnescapeXMLValue(string xmlString)
        {
            if (xmlString == null)
                return "";

            return xmlString.Replace("'", "&apos;").Replace("\"", "&quot;").Replace(">", "&gt;").Replace("<", "&lt;").Replace("&", "&amp;");
        }

        public static bool InsertFAQLevelOne(string groupname, string username)
        {
            try
            {
                var sqlconn = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
                using (var conn = new SqlConnection(sqlconn))
                {
                    SqlCommand command = new SqlCommand("TVSI_sINSERT_FAQ_LEVEL_ONE", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@name_group", groupname);
                    command.Parameters.AddWithValue("@createby", username);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(RPTUtils), e.Message);
                Logger.Debug(typeof(RPTUtils), e.InnerException);
                return false;
            }
        }
        
        public static bool InsertFAQLevelTwo(string groupname, string username,string id)
        {
            try
            {
                var sqlconn = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
                using (var conn = new SqlConnection(sqlconn))
                {
                    SqlCommand command = new SqlCommand("TVSI_sINSERT_FAQ_LEVEL_TWO", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@id", id);
                    command.Parameters.AddWithValue("@name_group", groupname);
                    command.Parameters.AddWithValue("@createby", username);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(RPTUtils), e.Message);
                Logger.Debug(typeof(RPTUtils), e.InnerException);
                return false;
            }
        }

        public static List<LevelOneModel> SelectLevelOne()
        {
            try
            {
                var listLevelOne = new List<LevelOneModel>();
                var conn = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
                using ( var sqlconn = new SqlConnection(conn))
                {
                    SqlCommand command = null;
                    SqlDataReader drd;
                    command =
                        new SqlCommand(
                            "SELECT group_name NameGroup, faq_one_id Id FROM TVSI_FAQ_LEVEL_ONE WHERE status = 1",sqlconn);
                    command.CommandType = CommandType.Text;
                    command.Connection.Open();
                    drd = command.ExecuteReader();
                    while (drd.Read())
                    {
                        listLevelOne.Add(new LevelOneModel()
                        {
                            Id = drd["Id"].ToString(),
                            NameGroup = drd["NameGroup"].ToString()
                        });
                    }
                    return listLevelOne;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(RPTUtils), "SelectLevelOne " + e.Message);
                Logger.Debug(typeof(RPTUtils),"SelectLevelOne " + e.InnerException);
                return null;
            }
        }

        public static bool GetGroupNameOne(string name)
        {
            try
            {
                var conn = ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ToString();
                using ( var sqlconn = new SqlConnection(conn))
                {
                    SqlCommand command = null;
                    SqlDataReader drd;
                    command =
                        new SqlCommand(
                            "SELECT group_name FROM TVSI_FAQ_LEVEL_ONE WHERE group_name = @groupname",sqlconn);
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add("@groupname", name);
                    command.Connection.Open();
                    drd = command.ExecuteReader();
                    if (drd.Read())
                        return true;
                    
                    return false;
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(RPTUtils), e.Message);
                Logger.Debug(typeof(RPTUtils), e.InnerException);
                return false;
            }
        }

        public static string GetUserInfo(string username)
        {
            try
            {
                using ( var sqlconn = new SqlConnection(ConnectionFactory.EmsConnectionString))
                {
                    SqlCommand command = null;
                    SqlDataReader drd;
                    command =
                        new SqlCommand(
                            "select ho_ten from TVSI_THONG_TIN_TRUY_CAP_CHI_TIET where id_he_thong = @UserDomain",sqlconn);
                    command.CommandType = CommandType.Text;
                    command.Parameters.Add("@UserDomain", username);
                    command.Connection.Open();
                    drd = command.ExecuteReader();
                    if (drd.Read())
                        return drd["ho_ten"].ToString();
                    
                    return "";
                }
            }
            catch (Exception e)
            {
                Logger.Debug(typeof(RPTUtils), e.Message);
                Logger.Debug(typeof(RPTUtils), e.InnerException);
                throw;
            }
        }
        
    }
}
