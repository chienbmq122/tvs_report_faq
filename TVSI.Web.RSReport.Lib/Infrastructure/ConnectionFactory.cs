﻿using System.Configuration;

namespace TVSI.Web.RSReport.Lib.Infrastructure
{
    public static class ConnectionFactory
    {
        public static string EmsConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ConnectionString; }
        }
    }
}