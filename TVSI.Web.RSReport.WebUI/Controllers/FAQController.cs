﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TVSI.Web.RSReport.Lib.Models.FAQ;
using TVSI.Web.RSReport.Lib.Service;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public class FAQController : Controller
    { 
        private readonly IFAQLevelOneService _faq;
        private readonly ITotalQuestionService _total;
        private readonly IFAQService _faqService;
        private readonly IFAQLevelTwoService _faqlevelTwo;
        private readonly IFAQFavoritesService _favoritesService;

        public FAQController()
        {
            _faq = new FAQLevelOneService();
            _total = new TotalQuestionService();
            _faqService = new FAQService();
            _faqlevelTwo = new FAQLevelTwoService();
            _favoritesService = new FAQFavoritesService();
        }
        [HttpPost]
        public ActionResult FAQ_CAP_DO_CAU_HOI_CAP_1(LevelOneModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");

            if (RPTUtils.GetGroupNameOne(model.NameGroup))
            {
                TempData["AlertMessExsOne"] = "Tên nhóm đã tồn tại";
                return RedirectToAction("FAQ_THEM_MOI_CAP_DO_CAU_HOI", "FAQ");
            }

            if (!string.IsNullOrEmpty(model.NameGroup))
            {
                bool savebool;
                savebool = RPTUtils.InsertFAQLevelOne(model.NameGroup,Session["UserProfile"].ToString());
                if(savebool)
                    TempData["AlertMess"] = "Thành công";
                else
                    TempData["AlertMess"] = "Thất bại";
                    
            }
            ViewBag.ActiveAddList = "active";
            return RedirectToAction("FAQ_THEM_MOI_CAP_DO_CAU_HOI", "FAQ");
        }

        [HttpPost]
        public ActionResult FAQ_CAP_DO_CAU_HOI_CAP_2(string id, string namegroup)
        {
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(namegroup))
            {
                bool savebool;
                savebool = RPTUtils.InsertFAQLevelTwo(namegroup,Session["UserName"].ToString(),id);
                if(savebool)
                    TempData["AlertMess"] = "Thành công";
                else
                    TempData["AlertMess"] = "Thất bại";
                
            }

            return RedirectToAction("FAQ_THEM_MOI_CAP_DO_CAU_HOI", "FAQ");
        }
        
        
        [HttpGet]
        public async Task<ActionResult> FAQ_CAP_DO_CAU_HOI(LevelOneModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            const int PageSize = 5;
            var list = await _faq.GetListFAQLevelTwo(model.KeySearch);
            var count = list.Count();
            ViewBag.FAQLevelTwo  = list.Skip(model.Page * PageSize).Take(PageSize).ToList();
            ViewBag.FAQLevelTwoMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQLevelTwoPage = model.Page;
            ViewBag.Admin = "active";
            await DropDownlistLevel();
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> FAQ_SearchFAQ(int id,string keysearch = "")
        {
            var searchList = await _total.FAQKeySearch(id, keysearch);
            ViewBag.FileName = await _faqService.GetFileName();
            ViewBag.Url = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_UPLOAD"]);
            ViewBag.FAQFavolist = await _favoritesService.GetFavoritelist(Session["UserName"].ToString());
            return PartialView("_DetailListInfo", searchList);
        }
        
        [HttpPost]
        public async Task<ActionResult> FAQ_CAP_DO_CAU_HOI(FAQQuestionLevelsModel model)
        {
            const int PageSize = 5;
            var list = await _faq.GetListFAQLevelTwo(model.KeySearch);
            var count = list.Count();
            ViewBag.FAQLevelTwo  = list.Skip(model.Page * PageSize).Take(PageSize).ToList();
            ViewBag.FAQLevelTwoMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQLevelTwoPage = model.Page;
            await DropDownlistLevel();
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> FAQ_TONG_QUAN_CAU_HOI(string keysearch = "",int page = 0)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            ViewBag.FAQLevelOne = await _total.FAQLevelOneList();
            ViewBag.FAQLevelTwo = await _total.FAQLevelTwoList();
            ViewBag.FAQList = await _total.FAQGetListData();
            ViewBag.FAQFavolist = await _favoritesService.GetFavoritelist(Session["UserName"].ToString());
            const int PageSize = 5;
            /*
            var list = await _faq.GetListFAQLevelTwo(keysearch);
            */
            ViewBag.ActiveList = "active";
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> FAQ_DANH_SACH_YEU_THICH()
        {
            return ViewBag();
        }

        [HttpGet]
        public async Task<ActionResult> FAQ_THEM_MOI_CAP_DO_CAU_HOI()
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            const int PageSize = 5;
            var list = await _faq.GetListFAQLevelTwo("");
            var count = list.Count();
            ViewBag.FAQLevelTwo  = list.Skip(0 * PageSize).Take(PageSize).ToList();
            ViewBag.FAQLevelTwoMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQLevelTwoPage = 0;
            await DropDownlistLevel();
            ViewBag.Admin = "active";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> FAQGetListInfo(int id)
        {
            var data = await _total.FAQDetailLevelTwoList(id);
            const int PageSize = 5;
            var list = await _faq.GetListFAQLevelTwo("");
            var count = list.Count();
            ViewBag.FAQLevelTwo  = list.Skip(0 * PageSize).Take(PageSize).ToList();
            ViewBag.FAQLevelTwoMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQLevelTwoPage = 0;
            ViewBag.FileName = await _faqService.GetFileName();
            ViewBag.Url = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_UPLOAD"]);
            ViewBag.FAQSuccess = 1;
            ViewBag.FAQFavolist = await _favoritesService.GetFavoritelist(Session["UserName"].ToString());

            return PartialView("_DetailListInfo", data);
        }



        [HttpGet]
        public async Task<ActionResult> FAQ_CAU_HOI_TRA_LOI(FAQModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            const int PageSize = 5;

            var list = await _faqService.GetListFAQ(model.KeySearch);
            var count = list.Count();
            ViewBag.ListDataFAQ = list.Skip(model.Page * PageSize).Take(PageSize).ToList();
            ViewBag.FAQMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQPage = model.Page;
            ViewBag.Admin = "active";
            await DropDownlistLevel();
            ViewBag.FileName = await _faqService.GetFileName();
            ViewBag.Url = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_UPLOAD"]);
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> FAQ_DELETE_FAQ_LEVEL_TWO(int id)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            if (id > 0)
            {
                var data = await _faqlevelTwo.FAQDeleteLevelTwo(id);
                if (data)
                {
                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("FAQ_CAP_DO_CAU_HOI", "FAQ");
                    return Json(new { status = 1, url = redirectUrl }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = 99 }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> FAQ_QUESTION_AND_ANSWER(int id)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if (id > 0)
            {
                var data = await _faqService.DeleteFAQ(id);
                if (data)
                {
                    var redirectUrl = new UrlHelper(Request.RequestContext).Action("FAQ_CAU_HOI_TRA_LOI", "FAQ");
                    return Json(new { status = 1, url = redirectUrl }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = 99 }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public async Task<ActionResult> FAQ_ADD_FAVORITES_LIST(int id)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if (id > 0)
            {
               var data =  await _favoritesService.AddFavorite(id,Session["UserName"].ToString());
               if (data)
               {
                   return Json(new { status = 1}, JsonRequestBehavior.AllowGet);
               }
            }
            return Json(new { status = 99 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> FAQ_REMOVE_FAVORITES_LIST(int id)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if (id > 0)
            {
                var data =  await _favoritesService.RemoveFavorite(id,Session["UserName"].ToString());
                if (data)
                {
                    return Json(new { status = 1}, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { status = 99 }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> FAQ_UPDATE_LEVEL_TWO(int id, string namegroup)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if (!string.IsNullOrEmpty(namegroup) && id > 0)
            {
                var data = await _faqlevelTwo.FAQUpdateLevelTwo(id, namegroup);
                if (data)
                {
                    TempData["SUCCESS"] = 1;
                    return RedirectToAction("FAQ_CAP_DO_CAU_HOI", "FAQ");
                }
            }
            TempData["SUCCESS"] = 99;
            return RedirectToAction("FAQ_CAP_DO_CAU_HOI", "FAQ");
        }    
        
        [HttpPost]
        public async Task<ActionResult> FAQ_UPDATE_QUESTION_AND_ASWER(FAQModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if (!string.IsNullOrEmpty(model.QuestionName) && model.id > 0 && model.Id_B > 0 && !string.IsNullOrEmpty(model.Answer))
            {
                var data = await _faqService.UpdateQuestionAndAnswer(model.id, model.QuestionName, model.Answer,
                    model.Id_B,model.File,model.FileID,Session["UserName"].ToString(),model.Hashtag);
                if (data)
                {
                    TempData["SUCCESS"] = 1;
                    return RedirectToAction("FAQ_CAU_HOI_TRA_LOI", "FAQ");
                }
            }
            TempData["SUCCESS"] = 99;
            return RedirectToAction("FAQ_CAU_HOI_TRA_LOI", "FAQ");
        }

        [HttpGet]
        public  async Task<ActionResult> FAQ_THEM_MOI_CAU_HOI_VA_TRA_LOI()
        {
            ViewBag.Admin = "active";
            await DropDownlistLevel();
            return  View();
        }

        [HttpGet]
        public async Task<ActionResult> FAQ_DANH_SACH_CAU_HOI_YEU_THICH(FAQFavoriteslistModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            const int PageSize = 5;
            var list = await _favoritesService.GetFavoriteslistall(model.KeySearch,Session["UserName"].ToString());
            var count = list.Count();
            ViewBag.FAQLevelTwo  = list.Skip(model.Page * PageSize).Take(PageSize).ToList();
            ViewBag.FAQLevelTwoMaxPage = (count / PageSize) - (count % PageSize == 0 ? 1 : 0);
            ViewBag.FAQLevelTwoPage = model.Page;
            ViewBag.FileName = await _faqService.GetFileName();
            ViewBag.Url = System.Web.HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FOLDER_UPLOAD"]);
            ViewBag.ActiveList = "active";
            await DropDownlistLevel();
            return View(model);
        }
        
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FAQ_THEM_MOI_CAU_HOI_VA_TRA_LOI(FAQLevelTwoModel model)
        {
            if (Session["UserProfile"] == null)
                return RedirectToAction("Logout", "User");
            
            if(!ModelState.IsValid)
            {
                return View(model);
            }
            
            ViewBag.Success = 1;
            TempData["Success"] = 1;
            
            await DropDownlistLevel();
            
            await _faqService.SaveQuestionsAndAnswers(model,Session["UserName"].ToString());
            
            return RedirectToAction("FAQ_THEM_MOI_CAU_HOI_VA_TRA_LOI");
        }
        public async Task DropDownlistLevel()
        {
            var listLevelOne = RPTUtils.SelectLevelOne();
            ViewBag.ListLevel = listLevelOne.Count > 0
                ? new SelectList(listLevelOne, "Id", "NameGroup")
                : new SelectList(new List<LevelOneModel>(), "Id", "NameGroup");
            var listLevelTwo = await _total.FAQLevelTwoList();
            ViewBag.ListLevelTwo = listLevelTwo.Count() > 0
                ? new SelectList(listLevelTwo, "Id", "GroupName")
                : new SelectList(new List<FAQLevelTwoModel>(), "Id", "GroupName");     

            
        }

        public async Task<ActionResult> DownloadFile(string filename)
        {
            try
            {
                var path = ConfigurationManager.AppSettings["FOLDER_UPLOAD"];
                var filepath = System.Web.HttpContext.Current.Server.MapPath(path + filename);
                var filedata = System.IO.File.ReadAllBytes(filepath);
                var contentType = MimeMapping.GetMimeMapping(filepath);
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = filename,
                    Inline = true,
                };
                Response.AppendHeader("Content-Disposition", cd.ToString());

                return File(filedata, contentType);
                
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        public async Task<ActionResult> GetGroupNameLevelOne(int id)
        {
            var nameLevel = await _total.GetGroupNameLvOne(id);
            return Json(new { data = nameLevel,status = 1 },JsonRequestBehavior.AllowGet);
        }
    }
}
