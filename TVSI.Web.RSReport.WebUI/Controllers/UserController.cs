﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TVSI.Utility;
using TVSI.Web.RSReport.Lib.Models;
using TVSI.Web.RSReport.Lib.Service;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public class UserController : Controller
    {
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                
                string strUserName = ConfigurationManager.AppSettings["USER_LOGIN"].ToString();
                string[] arrListStr = strUserName.Split(',');
                foreach (var item in arrListStr)
                {
                    if (item == model.UserName)
                    {
                        var isLoginDomainSuccess = CheckLdap(model.UserName, model.Password);
                        Logger.Info(typeof(UserController), string.Format("Login(userName={0}): ", model.UserName) + isLoginDomainSuccess + "-" + model.UserName + "-"+ model.Password);
                        if (isLoginDomainSuccess)
                        {
                            this.Session["UserProfile"] = RPTUtils.GetUserInfo(model.UserName);
                            this.Session["UserName"] = model.UserName;
                            return RedirectToAction("Index", "Home");
                        }
                        
                    }
                }
                ModelState.AddModelError("", "Tên hoặc mật khẩu không đúng.");
                return View(model);
            }
            catch (HttpRequestException ex)
            {
                Logger.Error(typeof(UserController), string.Format("Login(userName={0}): ", model.UserName) + ex.Message);
                ModelState.AddModelError("", "Tên hoặc mật khẩu không đúng.");
                return View(model);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            Logger.Info(typeof(UserController), "User - loggout at IP " + Request.UserHostAddress);
            FormsAuthentication.SignOut();
            Session.Clear();
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            return RedirectToAction("Login", "User");
        }

        public bool CheckLdap(string username, string pass)
        {
            string ldapServerIp = ConfigurationManager.AppSettings["LDAP_SERVER_IP"];
            // This function requires Imports System.Net and Imports System.DirectoryServices.Protocols
            System.DirectoryServices.Protocols.LdapConnection ldapConnection = new System.DirectoryServices.Protocols.LdapConnection(new LdapDirectoryIdentifier(ldapServerIp));
            //  Use the TimeSpan constructor to specify...
            //  ... days, hours, minutes, seconds, milliseconds.
            TimeSpan mytimeout = new TimeSpan(0, 0, 0, 1);
            try
            {
                ldapConnection.AuthType = AuthType.Negotiate;
                ldapConnection.AutoBind = false;
                ldapConnection.Timeout = mytimeout;
                ldapConnection.Credential = new NetworkCredential(username, pass);
                ldapConnection.Bind();
                return true;
                // LdapOK = true;
                // ldapConnection.Dispose();
            }
            catch (LdapException e)
            {
                return false;
                //onsole.WriteLine((e.GetType.ToString + (":" + e.Message)));
                //  LdapOK = false;
            }
        }
    }
}
