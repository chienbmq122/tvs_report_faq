﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_LICH_SU_KIEN()
        {
            return View();
        }

        public FileContentResult ExportEvents(string Code, string fromDate, string toDate)
        {

            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_LICH_SU_KIEN",
                    new { Code, Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelEventsFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_LICH_SU_KIEN.xlsx");
            }

        }

        private byte[] CreateExcelEventsFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_LICH_SU_KIEN");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Ngày thông báo";
                worksheet.Cells[1, 3].Value = "Tên sự kiện";
                worksheet.Cells[1, 4].Value = "Mã sự kiện";
                worksheet.Cells[1, 5].Value = "Ngày thực hiện";
                worksheet.Cells[1, 6].Value = "Ngày đăng ký cuối cùng";
                worksheet.Cells[1, 7].Value = "Ngày giao dịch không hưởng quyền";
                worksheet.Cells[1, 8].Value = "Mô tả";
                

                using (var range = worksheet.Cells[1, 1, 1, 8])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.AnDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.EventName;
                    worksheet.Cells[row, 4].Value = item.EventCode;
                    worksheet.Cells[row, 5].Value = item.ExDate;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 6].Value = item.RegFinalDate;
                    worksheet.Cells[row, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 7].Value = item.ExRigthDate;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 8].Value = item.EventDesc;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 8])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 8; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}