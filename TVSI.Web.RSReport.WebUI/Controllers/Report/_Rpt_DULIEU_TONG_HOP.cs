﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_DU_LIEU_TONG_HOP()
        {
            return View();
        }

        public FileContentResult ExportAllData(string yearReport, string lengthReport)
        {


            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_DU_LIEU_TONG_HOP",
                    new { iyearReport = yearReport, ilengthReport = lengthReport },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelAllDataFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_DULIEU_TONG_HOP.xlsx");
            }

        }

        private byte[] CreateExcelAllDataFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_RPT_DULIEU_TONG_HOP");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Quý";
                worksheet.Cells[1, 3].Value = "Năm";
                worksheet.Cells[1, 4].Value = "Ngày cập nhật";
                worksheet.Cells[1, 5].Value = "Số CP lưu hành";
                worksheet.Cells[1, 6].Value = "Số CP niêm yết";
                worksheet.Cells[1, 7].Value = "Vốn điều lệ";
                worksheet.Cells[1, 8].Value = "Mệnh giá";
                worksheet.Cells[1, 9].Value = "Giá chào sàn";
                worksheet.Cells[1, 10].Value = "Số CĐ phổ thông";
                worksheet.Cells[1, 11].Value = "Số lượng NV";
                worksheet.Cells[1, 12].Value = "Số lượng ATM";
                worksheet.Cells[1, 13].Value = "Số CN cấp I";
                worksheet.Cells[1, 14].Value = "Số phòng GD";
                worksheet.Cells[1, 15].Value = "Free Float";
                worksheet.Cells[1, 16].Value = "Doanh thu";
                worksheet.Cells[1, 17].Value = "LNTT";
                worksheet.Cells[1, 18].Value = "LNST";
                worksheet.Cells[1, 19].Value = "Kế hoạch cả năm";
                worksheet.Cells[1, 20].Value = "Vốn điều lệ";
                worksheet.Cells[1, 21].Value = "Giá đóng cửa";
                worksheet.Cells[1, 22].Value = "Vốn hóa";
                worksheet.Cells[1, 23].Value = "Sở hữu NN";
                worksheet.Cells[1, 24].Value = "EPS4Quy";
                worksheet.Cells[1, 25].Value = "PE";
                worksheet.Cells[1, 26].Value = "PB";
                worksheet.Cells[1, 27].Value = "Lãi cổ tức";
                worksheet.Cells[1, 28].Value = "ROE";
                worksheet.Cells[1, 29].Value = "ROA";
                worksheet.Cells[1, 30].Value = "Doanh số";
                worksheet.Cells[1, 31].Value = "Lãi hoạt động";
                worksheet.Cells[1, 32].Value = "Lãi ròng";
                worksheet.Cells[1, 33].Value = "Tổng tài sản";
                worksheet.Cells[1, 34].Value = "Tổng công nợ";
                worksheet.Cells[1, 35].Value = "Vốn CSH";
                worksheet.Cells[1, 36].Value = "Tiền mặt";
                worksheet.Cells[1, 37].Value = "Vốn vay/VCSH";
                worksheet.Cells[1, 38].Value = "KLGD";
                worksheet.Cells[1, 39].Value = "Tăng/Giảm giá hôm nay";
                worksheet.Cells[1, 40].Value = "Tăng/Giảm giá 1 tháng";
                worksheet.Cells[1, 41].Value = "Tăng/Giảm giá 1 tháng";
                worksheet.Cells[1, 42].Value = "Tăng/Giảm giá 3 tháng";
                worksheet.Cells[1, 43].Value = "Tăng/Giảm giá 6 tháng";
                worksheet.Cells[1, 44].Value = "Tăng/Giảm giá từ đầu năm";
                worksheet.Cells[1, 45].Value = "Tăng/Giảm giá từ lúc cao nhất";
                worksheet.Cells[1, 46].Value = "Tăng/Giảm giá từ lúc thấp nhất";
                worksheet.Cells[1, 47].Value = "Vốn điều lệ";
                worksheet.Cells[1, 48].Value = "Giá trị sổ sách";
                worksheet.Cells[1, 49].Value = "DE";
                worksheet.Cells[1, 50].Value = "Sở hữu NN";
                worksheet.Cells[1, 51].Value = "Tăng/Giảm KL 7 ngày";
                worksheet.Cells[1, 52].Value = "Tăng/Giảm KL 1 tháng";
                worksheet.Cells[1, 53].Value = "Tăng/Giảm KL 3 tháng";
                worksheet.Cells[1, 54].Value = "Cổ tức gần nhất";
                worksheet.Cells[1, 55].Value = "Trượt 4 quý";
                worksheet.Cells[1, 56].Value = "Ebit";
                worksheet.Cells[1, 57].Value = "Ebitda";
                using (var range = worksheet.Cells[1, 1, 1, 57])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.LengthReport;
                    worksheet.Cells[row, 3].Value = item.YearReport;
                    worksheet.Cells[row, 4].Value = item.NgayCapNhat;
                    worksheet.Cells[row, 4].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 5].Value = item.ShareCirculate;
                    worksheet.Cells[row, 6].Value = item.ShareIssue;
                    worksheet.Cells[row, 7].Value = item.CapitalFund;
                    worksheet.Cells[row, 8].Value = item.ParValue;
                    worksheet.Cells[row, 9].Value = item.PriceListed;
                    worksheet.Cells[row, 10].Value = item.NumberOfCDPT;
                    worksheet.Cells[row, 11].Value = item.NumberOfEmployees;
                    worksheet.Cells[row, 12].Value = item.NumberOfATM;
                    worksheet.Cells[row, 13].Value = item.NumberOfSub1;
                    worksheet.Cells[row, 14].Value = item.NumberOfTradeStation;
                    worksheet.Cells[row, 15].Value = item.Freefloat;
                    worksheet.Cells[row, 16].Value = item.DoanhThu;
                    worksheet.Cells[row, 17].Value = item.LoiNhuanTruocThue;
                    worksheet.Cells[row, 18].Value = item.LoiNhuanSauThue;
                    worksheet.Cells[row, 19].Value = item.KH_CaNam;
                    worksheet.Cells[row, 20].Value = item.VonDieuLe;
                    worksheet.Cells[row, 21].Value = item.GiaDongCua;
                    worksheet.Cells[row, 22].Value = item.VonHoa;
                    worksheet.Cells[row, 23].Value = item.SoHuuNuocNgoai;
                    worksheet.Cells[row, 24].Value = item.EPS4Quy;
                    worksheet.Cells[row, 25].Value = item.PE;
                    worksheet.Cells[row, 26].Value = item.PB;
                    worksheet.Cells[row, 27].Value = item.LaiCoTuc;
                    worksheet.Cells[row, 28].Value = item.ROE;
                    worksheet.Cells[row, 29].Value = item.ROA;
                    worksheet.Cells[row, 30].Value = item.DoanhSo;
                    worksheet.Cells[row, 31].Value = item.LaiHoatDong;
                    worksheet.Cells[row, 32].Value = item.LaiDong;
                    worksheet.Cells[row, 33].Value = item.TongTaiSan;
                    worksheet.Cells[row, 34].Value = item.TongCongNo;
                    worksheet.Cells[row, 35].Value = item.VonCSH;
                    worksheet.Cells[row, 36].Value = item.TienMat;
                    worksheet.Cells[row, 37].Value = item.VonVayVonCSH;
                    worksheet.Cells[row, 38].Value = item.KLGD;
                    worksheet.Cells[row, 39].Value = item.GiaHomNay;
                    worksheet.Cells[row, 40].Value = item.Gia7Ngay;
                    worksheet.Cells[row, 41].Value = item.Gia30Ngay;
                    worksheet.Cells[row, 42].Value = item.Gia90Ngay;
                    worksheet.Cells[row, 43].Value = item.Gia180Ngay;
                    worksheet.Cells[row, 44].Value = item.GiaTuDauNam;
                    worksheet.Cells[row, 45].Value = item.GiaTuLucCaoNhat;
                    worksheet.Cells[row, 46].Value = item.GiaTuLucThapNhat;
                    worksheet.Cells[row, 47].Value = item.VonDieuLe;
                    worksheet.Cells[row, 48].Value = item.BookValue;
                    worksheet.Cells[row, 49].Value = item.DE;
                    worksheet.Cells[row, 50].Value = item.SoHuuNhanuoc;
                    worksheet.Cells[row, 51].Value = item.KL7Ngay;
                    worksheet.Cells[row, 52].Value = item.KL30Ngay;
                    worksheet.Cells[row, 53].Value = item.KL90Ngay;
                    worksheet.Cells[row, 54].Value = item.CoTucGanNhat;
                    worksheet.Cells[row, 55].Value = item.Truot4Quy;
                    worksheet.Cells[row, 56].Value = item.Ebit;
                    worksheet.Cells[row, 57].Value = item.Ebitda;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 57])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 57; i++)
                {
                    for (int j = 1; j <= list.Count()+1; j++)
                    {
                        if(i >= 4)
                            worksheet.Cells[j, i].Style.Numberformat.Format = "#,###";
                    }

                        worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}