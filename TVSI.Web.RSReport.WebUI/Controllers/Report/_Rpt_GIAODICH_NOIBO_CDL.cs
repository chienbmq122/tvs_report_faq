﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_GIAODICH_NOIBO_CDL()
        {
            return View();
        }

        public FileContentResult ExportGiaoDichNB(string Code, string fromDate, string toDate)
        {

            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_GIAODICH_NOIBO_CODONGLON",
                    new { Code, Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelShareholdersFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_GIAODICH_NOIBO_CODONGLON.xlsx");
            }

        }

        private byte[] CreateExcelShareholdersFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_GIAODICH_NOIBO_CODONGLON");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Ngày thông báo";
                worksheet.Cells[1, 3].Value = "Loại GD";
                worksheet.Cells[1, 4].Value = "Hình thức GD";
                worksheet.Cells[1, 5].Value = "Giao dịch qua CTCK";
                worksheet.Cells[1, 6].Value = "Người/Tổ chức TB";
                worksheet.Cells[1, 7].Value = "Chức vụ người TB";
                worksheet.Cells[1, 8].Value = "Tên người liên quan";
                worksheet.Cells[1, 9].Value = "Chức vụ người LQ";
                worksheet.Cells[1, 10].Value = "MQH người TB với người LQ";
                worksheet.Cells[1, 11].Value = "Loại GD";
                worksheet.Cells[1, 12].Value = "Số CP nắm giữ HT";
                worksheet.Cells[1, 13].Value = "Số CP đăng ký";
                worksheet.Cells[1, 14].Value = "Thực tế GD";
                worksheet.Cells[1, 15].Value = "Số CP phát sinh trong TGGD";
                worksheet.Cells[1, 16].Value = "Số CP sau GD";
                worksheet.Cells[1, 17].Value = "% sở hữu";
                worksheet.Cells[1, 18].Value = "Ngày bắt đầu";
                worksheet.Cells[1, 19].Value = "Ngày kết thúc";
                worksheet.Cells[1, 20].Value = "Tình trạng";
                worksheet.Cells[1, 21].Value = "Mục đích";
                worksheet.Cells[1, 22].Value = "Thông tin thêm";

                using (var range = worksheet.Cells[1, 1, 1, 22])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.AnDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.DealMethod;
                    worksheet.Cells[row, 4].Value = item.TradeMethod;
                    worksheet.Cells[row, 5].Value = item.CTCK_ID;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 6].Value = item.Notifier;
                    worksheet.Cells[row, 7].Value = item.NotifierFunction;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 8].Value = item.RelationNotifier;
                    worksheet.Cells[row, 8].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 9].Value = item.RelationFunction;
                    worksheet.Cells[row, 9].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 10].Value = item.ConcernNotifier;
                    worksheet.Cells[row, 10].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 11].Value = item.Action;
                    worksheet.Cells[row, 12].Value = item.CurrentShares;
                    worksheet.Cells[row, 12].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 13].Value = item.RegQuantity;
                    worksheet.Cells[row, 13].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 14].Value = item.Quantity;
                    worksheet.Cells[row, 14].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 15].Value = item.ShareSpring;
                    worksheet.Cells[row, 15].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 16].Value = item.ShareAfterTrade;
                    worksheet.Cells[row, 16].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 17].Value = item.OwnerShip;
                    worksheet.Cells[row, 17].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 18].Value = item.StartDate;
                    worksheet.Cells[row, 18].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 19].Value = item.EndDate;
                    worksheet.Cells[row, 19].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 20].Value = item.TradingStatus;
                    worksheet.Cells[row, 21].Value = item.Goal;
                    worksheet.Cells[row, 22].Value = item.AddInfo;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 22])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 22; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}