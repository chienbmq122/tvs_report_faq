﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Models;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {
        public ActionResult Rpt_NDTNN_HSX()
        {
            return View();
        }

        public FileContentResult ExportNdtnnHsx(string fromDate, string toDate)
        {
            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query<GdnnHsxViewModel>("TVSI_sRS_NDTNN_HSX",
                    new { Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure).ToList();

                var fileContent = CreateExcelFileHSX(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_NDTNN_HSX.xlsx");
            }

        }

        private byte[] CreateExcelFileHSX(List<GdnnHsxViewModel> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_NDTTNN_HSX");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "KL Mua";
                worksheet.Cells[1, 3].Value = "Giá trị mua";
                worksheet.Cells[1, 4].Value = "KL Bán";
                worksheet.Cells[1, 5].Value = "Giá trị bán";
                worksheet.Cells[1, 6].Value = "KL Mua Ròng";
                worksheet.Cells[1, 7].Value = "GT Mua Ròng";

                using (var range = worksheet.Cells[1, 1, 1, 7])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Code;
                    worksheet.Cells[row, 2].Value = item.KLMua;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 3].Value = item.GTMua;
                    worksheet.Cells[row, 3].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 4].Value = item.KLBan;
                    worksheet.Cells[row, 4].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 5].Value = item.GTBan;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 6].Value = item.KLMuaRong;
                    worksheet.Cells[row, 6].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 7].Value = item.GTMuaRong;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "#,###";
                    row++;
                }

                if (list.Count > 0)
                {
                    using (var range = worksheet.Cells[2, 1, list.Count + 1, 7])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 7; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}