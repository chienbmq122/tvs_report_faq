﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_DU_LIEU_TONG_HOP_INSURANCE()
        {
            return View();
        }

        public FileContentResult ExportAllDataInsurance(string lengthReport, string yearReport)
        {


            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_DU_LIEU_TONG_HOP_BAO_HIEM",
                    new {lengthReport = lengthReport, yearReport = yearReport },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelAllDataInsuranceFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_DULIEU_TONG_HOP_BH.xlsx");
            }

        }

        private byte[] CreateExcelAllDataInsuranceFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_RPT_DULIEU_TONG_HOP_BH");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Sàn GD";
                worksheet.Cells[1, 3].Value = "Thu phí bảo hiểm";
                worksheet.Cells[1, 4].Value = "Thu phí nhận tái bảo hiểm";
                worksheet.Cells[1, 5].Value = "Phí nhượng tại bảo hiểm";
                worksheet.Cells[1, 6].Value = "Chuyển phí nhượng tái bảo hiểm";
                worksheet.Cells[1, 7].Value = "Giảm phí";
                worksheet.Cells[1, 8].Value = "Hoàn phí";
                worksheet.Cells[1, 9].Value = "Các khoản giảm trừ khác";
                worksheet.Cells[1, 10].Value = "Tăng do dự phòng phí chưa được hưởng và dự phòng toán học (trước 2014)";
                worksheet.Cells[1, 11].Value = "Thu hoa hồng nhượng tái bảo hiểm";
                worksheet.Cells[1, 12].Value = "Thu nhập khác hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 13].Value = "Thu khác nhận tái bảo hiểm";
                worksheet.Cells[1, 14].Value = "Thu khác nhượng tái bảo hiểm";
                worksheet.Cells[1, 15].Value = "Thu hoạt động khác";
                worksheet.Cells[1, 16].Value = "Doanh thu thuần từ hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 17].Value = "Chi bồi thường bảo hiểm gốc và chi trả đáo hạn";
                worksheet.Cells[1, 18].Value = "Chi bồi thường nhận tái bảo hiểm";
                worksheet.Cells[1, 19].Value = "Các khoản giảm trừ chi phí";
                worksheet.Cells[1, 20].Value = "Thu bồi thường nhượng tái bảo hiểm (trước 2014)";
                worksheet.Cells[1, 21].Value = "Thu đòi người thứ ba";
                worksheet.Cells[1, 22].Value = "Thu xử lý hàng bồi thường 100%";
                worksheet.Cells[1, 23].Value = "Bồi thường thuộc trách nhiệm giữ lại";
                worksheet.Cells[1, 24].Value = "Tăng /(giảm) dự phòng bồi thường";
                worksheet.Cells[1, 25].Value = "Chi bồi thường tư quỹ dao động lớn";
                worksheet.Cells[1, 26].Value = "Trích dự phòng dao động lớn";
                worksheet.Cells[1, 27].Value = "Chi khác hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 28].Value = "Chi khác hoạt động bảo hiểm gốc";
                worksheet.Cells[1, 29].Value = "Chi hoa hồng bảo hiểm gốc";
                worksheet.Cells[1, 30].Value = "Chi đòi người thứ 3";
                worksheet.Cells[1, 31].Value = "Chi xử lý hàng bồi thường 100%";
                worksheet.Cells[1, 32].Value = "Chi đề phòng hạn chế tổn thất";
                worksheet.Cells[1, 33].Value = "Chi giám định , chi đánh giá rủi ro đối tượng được bảo hiểm, chi khác";
                worksheet.Cells[1, 34].Value = "Chi khác";
                worksheet.Cells[1, 35].Value = "Chi khác nhận tái bảo hiểm khác";
                worksheet.Cells[1, 36].Value = "Chi nhượng tái bảo hiểm";
                worksheet.Cells[1, 37].Value = "Chi phí trực tiếp kinh doanh hoạt động khác";
                worksheet.Cells[1, 38].Value = "Tổng chi trực tiếp hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 39].Value = "Lợi nhuận gộp hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 40].Value = "Thu nhập từ hoạt động ngân hàng";
                worksheet.Cells[1, 41].Value = "Chi phí của hoạt động ngân hàng";
                worksheet.Cells[1, 42].Value = "Thu nhập thuần từ hoạt động ngân hàng";
                worksheet.Cells[1, 43].Value = "Thu nhập từ hoạt động khác";
                worksheet.Cells[1, 44].Value = "Chi phí hoạt động khác";
                worksheet.Cells[1, 45].Value = "Thu nhập thuần từ các hoạt động khác";
                worksheet.Cells[1, 46].Value = "Chi phí bán hàng (trước 2014)";
                worksheet.Cells[1, 47].Value = "Chi phí quản lý doanh nghiệp";
                worksheet.Cells[1, 48].Value = "Chi phí quản lý doanh nghiệp liên quan trực tiếp đến hoạt động bảo hiểm";
                worksheet.Cells[1, 49].Value = "Chi phí quản lý liên quan trực tiếp đến hoạt động ngân hàng";
                worksheet.Cells[1, 50].Value = "Chi phí quản lý doanh nghiệp liên quan đến các lĩnh vực khác";
                worksheet.Cells[1, 51].Value = "Lợi nhuận thuần hoạt động kinh doanh bảo hiểm";
                worksheet.Cells[1, 52].Value = "Lợi nhuận thuần hoạt đông ngân hàng";
                worksheet.Cells[1, 53].Value = "Lợi nhuận thuần từ các hoạt động khác";
                worksheet.Cells[1, 54].Value = "Doanh thu hoạt động tài chính";
                worksheet.Cells[1, 55].Value = "Chi phí hoạt động tài chính";
                worksheet.Cells[1, 56].Value = "Lợi nhuận hoạt động tài chính";
                worksheet.Cells[1, 57].Value = "Thu nhập hoạt động khác";
                worksheet.Cells[1, 58].Value = "Chi phí hoạt động khác";
                worksheet.Cells[1, 59].Value = "Lợi nhuận hoạt động khác";
                worksheet.Cells[1, 60].Value = "Phần lợi nhuận từ đầu tư vào công ty liên kết, liên doanh";
                worksheet.Cells[1, 61].Value = "Tổng lợi nhuận kế toán trước thuế";
                worksheet.Cells[1, 62].Value = "Dự phòng đảm bảo cân đối";
                worksheet.Cells[1, 63].Value = "Chi phí thuế thu nhập doanh nghiệp trong năm";
                worksheet.Cells[1, 64].Value = "Lợi nhuận sau thuế thu nhập doanh nghiệp";
                worksheet.Cells[1, 65].Value = "Lợi ích của cổ đông thiểu số";
                worksheet.Cells[1, 66].Value = "Lợi nhuận sau thuế của chủ sở hữu, tập đoàn";
                worksheet.Cells[1, 67].Value = "Lãi cơ bản trên mỗi cổ phiếu";
                worksheet.Cells[1, 68].Value = "TÀI SẢN NGẮN HẠN";
                worksheet.Cells[1, 69].Value = "Tiền và các khoản tương đương tiền";
                worksheet.Cells[1, 70].Value = "Tiền";
                worksheet.Cells[1, 71].Value = "Các khoản tương đương tiền";
                worksheet.Cells[1, 72].Value = "Đầu tư tài chính ngắn hạn";
                worksheet.Cells[1, 73].Value = "Chứng khoán kinh doanh";
                worksheet.Cells[1, 74].Value = "Dự phòng giảm giá chứng khoán kinh doanh";
                worksheet.Cells[1, 75].Value = "Các khoản phải thu";
                worksheet.Cells[1, 76].Value = "Phải thu của khách hàng";
                worksheet.Cells[1, 77].Value = "Trả trước cho người bán";
                worksheet.Cells[1, 78].Value = "Tạm ứng";
                worksheet.Cells[1, 79].Value = "Phải thu từ hoạt động đầu tư tài chính";
                worksheet.Cells[1, 80].Value = "Các khoản phải thu khác";
                worksheet.Cells[1, 81].Value = "Dự phòng các khoản phải thu khó đòi";
                worksheet.Cells[1, 82].Value = "Hàng tồn kho";
                worksheet.Cells[1, 83].Value = "Hàng tồn kho";
                worksheet.Cells[1, 84].Value = "Dự phòng giảm giá hàng tồn kho";
                worksheet.Cells[1, 85].Value = "Tài sản ngắn hạn khác";
                worksheet.Cells[1, 86].Value = "Chi phí trả trước ngắn hạn";
                worksheet.Cells[1, 87].Value = "Tài sản thiếu chờ xử lý";
                worksheet.Cells[1, 88].Value = "Thuế GTGT được khấu trừ";
                worksheet.Cells[1, 89].Value = "Thuế và các khoản phải thu của nhà nước";
                worksheet.Cells[1, 90].Value = "Các khoản cầm cố, ký cược, ký quỹ ngắn hạn";
                worksheet.Cells[1, 91].Value = "Tài sản ngắn hạn khác";
                worksheet.Cells[1, 92].Value = "CHO VAY VÀ ỨNG TRƯỚC CHO KHÁCH HÀNG";
                worksheet.Cells[1, 93].Value = "Cho vay và ứng trước cho khách hàng";
                worksheet.Cells[1, 94].Value = "Dự phòng rủi ro tín dụng";
                worksheet.Cells[1, 95].Value = "TÀI SẢN DÀI HẠN";
                worksheet.Cells[1, 96].Value = "Tài sản cố định";
                worksheet.Cells[1, 97].Value = "Tài sản cố định hữu hình";
                worksheet.Cells[1, 98].Value = "Nguyên giá";
                worksheet.Cells[1, 99].Value = "Giá trị hao mòn lũy kế";
                worksheet.Cells[1, 100].Value = "Tài sản cố định thuê tài chính";
                worksheet.Cells[1, 101].Value = "Nguyên giá";
                worksheet.Cells[1, 102].Value = "Giá trị hao mòn lũy kế";
                worksheet.Cells[1, 103].Value = "Tài sản cố định vô hình";
                worksheet.Cells[1, 104].Value = "Nguyên giá";
                worksheet.Cells[1, 105].Value = "Giá trị hao mòn lũy kế";
                worksheet.Cells[1, 106].Value = "Chi phí xây dựng cơ bản dở dang (trước 2015)";
                worksheet.Cells[1, 107].Value = "Bất động sản đầu tư";
                worksheet.Cells[1, 108].Value = "Nguyên giá";
                worksheet.Cells[1, 109].Value = "Giá trị hao mòn lũy kế";
                worksheet.Cells[1, 110].Value = "Các khoản đầu tư tài chính dài hạn";
                worksheet.Cells[1, 111].Value = "Đầu tư vào công ty con";
                worksheet.Cells[1, 112].Value = "Đầu tư vào công ty liên kết , liên doanh";
                worksheet.Cells[1, 113].Value = "Đầu tư dài hạn khác";
                worksheet.Cells[1, 114].Value = "Dự phòng giảm giá đầu tư tài chính dài hạn";
                worksheet.Cells[1, 115].Value = "Tài sản dài hạn khác";
                worksheet.Cells[1, 116].Value = "Chi phí trả trước dài hạn";
                worksheet.Cells[1, 117].Value = "Tài sản thuế thu nhập hoãn lại";
                worksheet.Cells[1, 118].Value = "Tài sản ký quỹ dài hạn";
                worksheet.Cells[1, 119].Value = "Tài sản dài hạn khác";
                worksheet.Cells[1, 120].Value = "TỔNG CỘNG TÀI SẢN";
                worksheet.Cells[1, 121].Value = "NỢ PHẢI TRẢ";
                worksheet.Cells[1, 122].Value = "Nợ ngắn hạn";
                worksheet.Cells[1, 123].Value = "Vay và nợ ngắn hạn";
                worksheet.Cells[1, 124].Value = "Phải trả thương mại";
                worksheet.Cells[1, 125].Value = "Người mua trả tiền trước";
                worksheet.Cells[1, 126].Value = "Thuế và các khoản phải nộp nhà nước";
                worksheet.Cells[1, 127].Value = "Phải trả người lao động";
                worksheet.Cells[1, 128].Value = "Chi phí phải trả";
                worksheet.Cells[1, 129].Value = "Các khoản phải trả, phải nộp khác";
                worksheet.Cells[1, 130].Value = "Quỹ khen thưởng phúc lợi";
                worksheet.Cells[1, 131].Value = "Tiền gửi của khách hàng và các tổ chức tín dụng khác";
                worksheet.Cells[1, 132].Value = "Tiền gửi của tổ chức tín dụng";
                worksheet.Cells[1, 133].Value = "Tiền gửi của khách hàng";
                worksheet.Cells[1, 134].Value = "Nợ dài hạn";
                worksheet.Cells[1, 135].Value = "Ký quỹ , ký cược dài hạn";
                worksheet.Cells[1, 136].Value = "Thuế thu nhập hoãn lại phải trả";
                worksheet.Cells[1, 137].Value = "Dự phòng trợ cấp mất việc làm";
                worksheet.Cells[1, 138].Value = "Dự phòng nghiệp vụ bảo hiểm (trước 2014)";
                worksheet.Cells[1, 139].Value = "Dụ phòng phí chưa được hưởng (trước 2014)";
                worksheet.Cells[1, 140].Value = "Dự phòng toán học (trước 2014)";
                worksheet.Cells[1, 141].Value = "Dự phòng bồi thường (trước 2014)";
                worksheet.Cells[1, 142].Value = "Dự phòng dao động lớn (trước 2014)";
                worksheet.Cells[1, 143].Value = "Dự phòng chia lãi (trước 2014)";
                worksheet.Cells[1, 144].Value = "Dự phòng đảm bảo cân đối (trước 2014)";
                worksheet.Cells[1, 145].Value = "VỐN CHỦ SỞ HỮU";
                worksheet.Cells[1, 146].Value = "Vốn chủ sở hữu";
                worksheet.Cells[1, 147].Value = "Vốn đầu tư của chủ sở hữu";
                worksheet.Cells[1, 148].Value = "Thặng dư vốn cổ phần";
                worksheet.Cells[1, 149].Value = "Cổ phiếu ngân quỹ";
                worksheet.Cells[1, 150].Value = "Vốn khác";
                worksheet.Cells[1, 151].Value = "Chênh lệch tỷ giá";
                worksheet.Cells[1, 152].Value = "Quỹ đầu tư phát triển";
                worksheet.Cells[1, 153].Value = "Quỹ dự phòng tài chính";
                worksheet.Cells[1, 154].Value = "Quỹ dự trữ bắt buộc";
                worksheet.Cells[1, 155].Value = "Lợi nhuận sau thuế chưa phân phối";
                worksheet.Cells[1, 156].Value = "Nguồn kinh phí và quỹ khác";
                worksheet.Cells[1, 157].Value = "Quỹ khen thưởng phúc lợi (trước 2010)";
                worksheet.Cells[1, 158].Value = "LỢI ÍCH CỔ ĐÔNG KHÔNG KIỂM SOÁT (trước 2015)";
                worksheet.Cells[1, 159].Value = "TỔNG CỘNG NGUỒN VỐN";
                worksheet.Cells[1, 160].Value = "1. Tiền thu phí bảo hiểm và thu lãi";
                worksheet.Cells[1, 161].Value = "2. Trả tiền cho người bán, người cung cấp dịch vụ";
                worksheet.Cells[1, 162].Value = "3. Tiền chi trả cho người lao động";
                worksheet.Cells[1, 163].Value = "4. Tiền chi trả lãi vay";
                worksheet.Cells[1, 164].Value = "5. Tiền đã nộp thuế thu nhập doanh nghiệp";
                worksheet.Cells[1, 165].Value = "6. Tiền thu khác từ hoạt động kinh doanh";
                worksheet.Cells[1, 166].Value = "7. Tiền chi khác cho hoạt động kinh doanh";
                worksheet.Cells[1, 167].Value = "Lưu chuyển tiền thuần từ hoạt động kinh doanh";
                worksheet.Cells[1, 168].Value = "1. Tiền chi để mua sắm - xây dựng tài sản cố định";
                worksheet.Cells[1, 169].Value = "2. Tiền thu từ thanh lý, nhượng bán tài sản cố định";
                worksheet.Cells[1, 170].Value = "3. Tiền chi cho vay, mua các công cụ nợ của các đơn vị khác";
                worksheet.Cells[1, 171].Value = "4. Tiền thu hồi cho vay, bán lại công cụ nợ các đơn vị khác";
                worksheet.Cells[1, 172].Value = "5.Tiền chi đầu tư góp vốn vào các đơn vị khác";
                worksheet.Cells[1, 173].Value = "6. Tiền thu hồi đầu tư góp vốn vào các đơn vị khác";
                worksheet.Cells[1, 174].Value = "7. Tiền thu lãi cho vay, cổ tức và lợi nhuận được chia";
                worksheet.Cells[1, 175].Value = "8. Tiền ủy thác đầu tư";
                worksheet.Cells[1, 176].Value = "9. Tiền rút vốn ủy thác đầu tư";
                worksheet.Cells[1, 177].Value = "Lưu chuyển tiền thuần từ hoạt động đầu tư";
                worksheet.Cells[1, 178].Value = "1. Tiền thu từ phát hành cổ phiếu, nhận góp vốn của chủ sở hữu";
                worksheet.Cells[1, 179].Value = "2. Tiền trả lãi cho chủ sở hữu, mua lại cổ phiếu";
                worksheet.Cells[1, 180].Value = "3. Tiền thu từ vay ngắn và dài hạn";
                worksheet.Cells[1, 181].Value = "4. Tiền trả nợ vay";
                worksheet.Cells[1, 182].Value = "5. Tiền nợ thuê tài chính";
                worksheet.Cells[1, 183].Value = "6. Tiền trả cổ tức";
                worksheet.Cells[1, 184].Value = "7. Tiền trả lại các nhà đầu tư và chuyển trả Bộ tài chính về thặng dư vốn thu từ cổ phần hóa";
                worksheet.Cells[1, 185].Value = "Lưu chuyển từ thuần từ hoạt động tài chính";
                worksheet.Cells[1, 186].Value = "Lưu chuyển tiền thuần trong kỳ";
                worksheet.Cells[1, 187].Value = "Tiền và các khoản tương đương tiền";
                worksheet.Cells[1, 188].Value = "Ảnh hưởng của biến động tỷ giá";
                worksheet.Cells[1, 189].Value = "Tiền và các khoản tương đương tiền cuối kỳ";

                using (var range = worksheet.Cells[1, 1, 1, 189])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.San;
                    worksheet.Cells[row, 3].Value = item.F9_92;
                    worksheet.Cells[row, 4].Value = item.F9_93;
                    worksheet.Cells[row, 5].Value = item.F9_94;
                    worksheet.Cells[row, 6].Value = item.F9_95;
                    worksheet.Cells[row, 7].Value = item.F9_96;
                    worksheet.Cells[row, 8].Value = item.F9_97;
                    worksheet.Cells[row, 9].Value = item.F9_98;
                    worksheet.Cells[row, 10].Value = item.F9_99;
                    worksheet.Cells[row, 11].Value = item.F9_100;
                    worksheet.Cells[row, 12].Value = item.F9_101;
                    worksheet.Cells[row, 13].Value = item.F9_102;
                    worksheet.Cells[row, 14].Value = item.F9_103;
                    worksheet.Cells[row, 15].Value = item.F9_104;
                    worksheet.Cells[row, 16].Value = item.F9_105;
                    worksheet.Cells[row, 17].Value = item.F9_106;
                    worksheet.Cells[row, 18].Value = item.F9_107;
                    worksheet.Cells[row, 19].Value = item.F9_108;
                    worksheet.Cells[row, 20].Value = item.F9_109;
                    worksheet.Cells[row, 21].Value = item.F9_110;
                    worksheet.Cells[row, 22].Value = item.F9_111;
                    worksheet.Cells[row, 23].Value = item.F9_112;
                    worksheet.Cells[row, 24].Value = item.F9_113;
                    worksheet.Cells[row, 25].Value = item.F9_114;
                    worksheet.Cells[row, 26].Value = item.F9_115;
                    worksheet.Cells[row, 27].Value = item.F9_116;
                    worksheet.Cells[row, 28].Value = item.F9_117;
                    worksheet.Cells[row, 29].Value = item.F9_118;
                    worksheet.Cells[row, 30].Value = item.F9_119;
                    worksheet.Cells[row, 31].Value = item.F9_120;
                    worksheet.Cells[row, 32].Value = item.F9_121;
                    worksheet.Cells[row, 33].Value = item.F9_122;
                    worksheet.Cells[row, 34].Value = item.F9_123;
                    worksheet.Cells[row, 35].Value = item.F9_124;
                    worksheet.Cells[row, 36].Value = item.F9_125;
                    worksheet.Cells[row, 37].Value = item.F9_125b;
                    worksheet.Cells[row, 38].Value = item.F9_126;
                    worksheet.Cells[row, 39].Value = item.F9_127;
                    worksheet.Cells[row, 40].Value = item.F9_128;
                    worksheet.Cells[row, 41].Value = item.F9_129;
                    worksheet.Cells[row, 42].Value = item.F9_130;
                    worksheet.Cells[row, 43].Value = item.F9_131;
                    worksheet.Cells[row, 44].Value = item.F9_132;
                    worksheet.Cells[row, 45].Value = item.F9_133;
                    worksheet.Cells[row, 46].Value = item.F9_134;
                    worksheet.Cells[row, 47].Value = item.F9_135;
                    worksheet.Cells[row, 48].Value = item.F9_136;
                    worksheet.Cells[row, 49].Value = item.F9_137;
                    worksheet.Cells[row, 50].Value = item.F9_138;
                    worksheet.Cells[row, 51].Value = item.F9_139;
                    worksheet.Cells[row, 52].Value = item.F9_140;
                    worksheet.Cells[row, 53].Value = item.F9_141;
                    worksheet.Cells[row, 54].Value = item.F9_142;
                    worksheet.Cells[row, 55].Value = item.F9_143;
                    worksheet.Cells[row, 56].Value = item.F9_144;
                    worksheet.Cells[row, 57].Value = item.F9_145;
                    worksheet.Cells[row, 58].Value = item.F9_146;
                    worksheet.Cells[row, 59].Value = item.F9_147;
                    worksheet.Cells[row, 60].Value = item.F9_148;
                    worksheet.Cells[row, 61].Value = item.F9_149;
                    worksheet.Cells[row, 62].Value = item.F9_150;
                    worksheet.Cells[row, 63].Value = item.F9_151;
                    worksheet.Cells[row, 64].Value = item.F9_152;
                    worksheet.Cells[row, 65].Value = item.F9_153;
                    worksheet.Cells[row, 66].Value = item.F9_154;
                    worksheet.Cells[row, 67].Value = item.F9_155;

                    worksheet.Cells[row, 68].Value = item.F9_1;
                    worksheet.Cells[row, 69].Value = item.F9_2;
                    worksheet.Cells[row, 70].Value = item.F9_3;
                    worksheet.Cells[row, 71].Value = item.F9_4;
                    worksheet.Cells[row, 72].Value = item.F9_5;
                    worksheet.Cells[row, 73].Value = item.F9_6;
                    worksheet.Cells[row, 74].Value = item.F9_7;
                    worksheet.Cells[row, 75].Value = item.F9_8;
                    worksheet.Cells[row, 76].Value = item.F9_9;
                    worksheet.Cells[row, 77].Value = item.F9_10;
                    worksheet.Cells[row, 78].Value = item.F9_11;
                    worksheet.Cells[row, 79].Value = item.F9_12;
                    worksheet.Cells[row, 80].Value = item.F9_13;
                    worksheet.Cells[row, 81].Value = item.F9_14;
                    worksheet.Cells[row, 82].Value = item.F9_15;
                    worksheet.Cells[row, 83].Value = item.F9_16;
                    worksheet.Cells[row, 84].Value = item.F9_17;
                    worksheet.Cells[row, 85].Value = item.F9_18;
                    worksheet.Cells[row, 86].Value = item.F9_19;
                    worksheet.Cells[row, 87].Value = item.F9_20;
                    worksheet.Cells[row, 88].Value = item.F9_21;
                    worksheet.Cells[row, 89].Value = item.F9_22;
                    worksheet.Cells[row, 90].Value = item.F9_23;
                    worksheet.Cells[row, 91].Value = item.F9_24;
                    worksheet.Cells[row, 92].Value = item.F9_25;
                    worksheet.Cells[row, 93].Value = item.F9_26;
                    worksheet.Cells[row, 94].Value = item.F9_27;
                    worksheet.Cells[row, 95].Value = item.F9_28;
                    worksheet.Cells[row, 96].Value = item.F9_29;
                    worksheet.Cells[row, 97].Value = item.F9_30;
                    worksheet.Cells[row, 98].Value = item.F9_31;
                    worksheet.Cells[row, 99].Value = item.F9_32;
                    worksheet.Cells[row, 100].Value = item.F9_33;
                    worksheet.Cells[row, 101].Value = item.F9_34;
                    worksheet.Cells[row, 102].Value = item.F9_35;
                    worksheet.Cells[row, 103].Value = item.F9_36;
                    worksheet.Cells[row, 104].Value = item.F9_37;
                    worksheet.Cells[row, 105].Value = item.F9_38;
                    worksheet.Cells[row, 106].Value = item.F9_39;
                    worksheet.Cells[row, 107].Value = item.F9_40;
                    worksheet.Cells[row, 108].Value = item.F9_41;
                    worksheet.Cells[row, 109].Value = item.F9_42;
                    worksheet.Cells[row, 110].Value = item.F9_43;
                    worksheet.Cells[row, 111].Value = item.F9_44;
                    worksheet.Cells[row, 112].Value = item.F9_45;
                    worksheet.Cells[row, 113].Value = item.F9_46;
                    worksheet.Cells[row, 114].Value = item.F9_47;
                    worksheet.Cells[row, 115].Value = item.F9_48;
                    worksheet.Cells[row, 116].Value = item.F9_49;
                    worksheet.Cells[row, 117].Value = item.F9_50;
                    worksheet.Cells[row, 118].Value = item.F9_51;
                    worksheet.Cells[row, 119].Value = item.F9_52;
                    worksheet.Cells[row, 120].Value = item.F9_53;
                    worksheet.Cells[row, 121].Value = item.F9_54;
                    worksheet.Cells[row, 122].Value = item.F9_55;
                    worksheet.Cells[row, 123].Value = item.F9_56;
                    worksheet.Cells[row, 124].Value = item.F9_57;
                    worksheet.Cells[row, 125].Value = item.F9_58;
                    worksheet.Cells[row, 126].Value = item.F9_59;
                    worksheet.Cells[row, 127].Value = item.F9_60;
                    worksheet.Cells[row, 128].Value = item.F9_61;
                    worksheet.Cells[row, 129].Value = item.F9_62;
                    worksheet.Cells[row, 130].Value = item.F9_89b;
                    worksheet.Cells[row, 131].Value = item.F9_63;
                    worksheet.Cells[row, 132].Value = item.F9_64;
                    worksheet.Cells[row, 133].Value = item.F9_65;
                    worksheet.Cells[row, 134].Value = item.F9_66;
                    worksheet.Cells[row, 135].Value = item.F9_67;
                    worksheet.Cells[row, 136].Value = item.F9_68;
                    worksheet.Cells[row, 137].Value = item.F9_69;
                    worksheet.Cells[row, 138].Value = item.F9_70;
                    worksheet.Cells[row, 139].Value = item.F9_71;
                    worksheet.Cells[row, 140].Value = item.F9_72;
                    worksheet.Cells[row, 141].Value = item.F9_73;
                    worksheet.Cells[row, 142].Value = item.F9_74;
                    worksheet.Cells[row, 143].Value = item.F9_75;
                    worksheet.Cells[row, 144].Value = item.F9_76;
                    worksheet.Cells[row, 145].Value = item.F9_77;
                    worksheet.Cells[row, 146].Value = item.F9_78;
                    worksheet.Cells[row, 147].Value = item.F9_79;
                    worksheet.Cells[row, 148].Value = item.F9_80;
                    worksheet.Cells[row, 149].Value = item.F9_81;
                    worksheet.Cells[row, 150].Value = item.F9_82;
                    worksheet.Cells[row, 151].Value = item.F9_83;
                    worksheet.Cells[row, 152].Value = item.F9_84;
                    worksheet.Cells[row, 153].Value = item.F9_85;
                    worksheet.Cells[row, 154].Value = item.F9_86;
                    worksheet.Cells[row, 155].Value = item.F9_87;
                    worksheet.Cells[row, 156].Value = item.F9_88;
                    worksheet.Cells[row, 157].Value = item.F9_89;
                    worksheet.Cells[row, 158].Value = item.F9_90;
                    worksheet.Cells[row, 159].Value = item.F9_91;
                    worksheet.Cells[row, 160].Value = item.F9_206;
                    worksheet.Cells[row, 161].Value = item.F9_207;
                    worksheet.Cells[row, 162].Value = item.F9_208;
                    worksheet.Cells[row, 163].Value = item.F9_209;
                    worksheet.Cells[row, 164].Value = item.F9_210;
                    worksheet.Cells[row, 165].Value = item.F9_211;
                    worksheet.Cells[row, 166].Value = item.F9_212;
                    worksheet.Cells[row, 167].Value = item.F9_213;
                    worksheet.Cells[row, 168].Value = item.F9_215;
                    worksheet.Cells[row, 169].Value = item.F9_216;
                    worksheet.Cells[row, 170].Value = item.F9_217;
                    worksheet.Cells[row, 171].Value = item.F9_218;
                    worksheet.Cells[row, 172].Value = item.F9_219;
                    worksheet.Cells[row, 173].Value = item.F9_220;
                    worksheet.Cells[row, 174].Value = item.F9_221;
                    worksheet.Cells[row, 175].Value = item.F9_222;
                    worksheet.Cells[row, 176].Value = item.F9_223;
                    worksheet.Cells[row, 177].Value = item.F9_224;
                    worksheet.Cells[row, 178].Value = item.F9_226;
                    worksheet.Cells[row, 179].Value = item.F9_227;
                    worksheet.Cells[row, 180].Value = item.F9_228;
                    worksheet.Cells[row, 181].Value = item.F9_229;
                    worksheet.Cells[row, 182].Value = item.F9_230;
                    worksheet.Cells[row, 183].Value = item.F9_231;
                    worksheet.Cells[row, 184].Value = item.F9_232;
                    worksheet.Cells[row, 185].Value = item.F9_233;
                    worksheet.Cells[row, 186].Value = item.F9_234;
                    worksheet.Cells[row, 187].Value = item.F9_235;
                    worksheet.Cells[row, 188].Value = item.F9_236;
                    worksheet.Cells[row, 189].Value = item.F9_237;          
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 189])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 189; i++)
                {
                    for (int j = 1; j <= list.Count() + 1; j++)
                    {
                        if (i >= 3)
                            worksheet.Cells[j, i].Style.Numberformat.Format = "#,###";
                    }

                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}