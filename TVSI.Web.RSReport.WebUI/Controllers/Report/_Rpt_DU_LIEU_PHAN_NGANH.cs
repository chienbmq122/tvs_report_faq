﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_DU_LIEU_PHAN_NGANH()
        {
            return View();
        }

        public FileContentResult ExportAllDataIndustry()
        {


            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_DU_LIEU_NGANH",
                    new {},
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelAllDataIndustryFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_DULIEU_NGANH.xlsx");
            }

        }

        private byte[] CreateExcelAllDataIndustryFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_RPT_DULIEU_NGANH");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Level-1";
                worksheet.Cells[1, 3].Value = "Tên ngành-1";
                worksheet.Cells[1, 4].Value = "Level-2";
                worksheet.Cells[1, 5].Value = "Tên ngành-2";
                worksheet.Cells[1, 6].Value = "Level-3";
                worksheet.Cells[1, 7].Value = "Tên ngành-3";
                worksheet.Cells[1, 8].Value = "Level-4";
                worksheet.Cells[1, 9].Value = "Tên ngành-4";
               
                using (var range = worksheet.Cells[1, 1, 1, 9])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.Level1;
                    worksheet.Cells[row, 3].Value = item.Name1;
                    worksheet.Cells[row, 4].Value = item.Level2;
                    worksheet.Cells[row, 5].Value = item.Name2;
                    worksheet.Cells[row, 6].Value = item.Level3;
                    worksheet.Cells[row, 7].Value = item.Name3;
                    worksheet.Cells[row, 8].Value = item.Level4;
                    worksheet.Cells[row, 9].Value = item.Name4;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 9])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 9; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}