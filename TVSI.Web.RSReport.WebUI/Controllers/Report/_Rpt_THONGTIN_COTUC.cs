﻿using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TVSI.Web.RSReport.Lib.Models;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_THONGTIN_COTUC()
        {
            return View();
        }

        public FileContentResult ExportThongtinCotuc(string Code, string fromDate, string toDate)
        {
            var dtxFrom = !string.IsNullOrEmpty(fromDate) ? DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                                                                 : (DateTime?)null;
            var dtxTo = !string.IsNullOrEmpty(toDate) ? DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                                                                 : (DateTime?)null;
            //if (!string.IsNullOrEmpty(fromDate))
            //{
            //    toDate = dtxFrom.ToString();
            //}
            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query<ThongtinCotucViewModel>("TVSI_sRS_THONG_TIN_TRA_CO_TUC",
                    new {Code,  Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure).ToList();

                var fileContent = CreateExcelFileHSX(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_THONGTIN_COTUC.xlsx");
            }

        }

        private byte[] CreateExcelFileHSX(List<ThongtinCotucViewModel> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_THONGTIN_COTUC");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Ticker";
                worksheet.Cells[1, 2].Value = "DeclarationDate";
                worksheet.Cells[1, 3].Value = "ExDividendDate";
                worksheet.Cells[1, 4].Value = "LastRegDate";
                worksheet.Cells[1, 5].Value = "PaymentDate";
                worksheet.Cells[1, 6].Value = "Value";
                worksheet.Cells[1, 7].Value = "Rate";
                worksheet.Cells[1, 8].Value = "CashYear";
                worksheet.Cells[1, 9].Value = "CashWave";
                worksheet.Cells[1, 10].Value = "AddInfo";

                using (var range = worksheet.Cells[1, 1, 1, 10])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.DeclarationDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.ExDividendDate;
                    worksheet.Cells[row, 3].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 4].Value = item.LastRegDate;
                    worksheet.Cells[row, 4].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 5].Value = item.PaymentDate;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 6].Value = item.Value;
                    worksheet.Cells[row, 6].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 7].Value = item.Rate;
                    worksheet.Cells[row, 8].Value = item.CashYear;
                    worksheet.Cells[row, 9].Value = item.CashWave;
                    worksheet.Cells[row, 10].Value = RPTUtils.UnescapeXMLValue(item.AddInfo);
                    //worksheet.Cells[row, 10].IsRichText = true;
                    row++;
                }

                if (list.Count > 0)
                {
                    using (var range = worksheet.Cells[2, 1, list.Count + 1, 10])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 10; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

       
    }
}