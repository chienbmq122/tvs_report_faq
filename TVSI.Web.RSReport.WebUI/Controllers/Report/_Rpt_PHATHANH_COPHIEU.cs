﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {
        public ActionResult Rpt_PHATHANH_COPHIEU()
        {
            return View();
        }

        public FileContentResult ExportPhatHanhCP(string Code, string fromDate, string toDate)
        {

            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_THONGTIN_PHATHANH_CP",
                    new { Code, Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelStockIssueFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_THONGTIN_PHATHANH_CP.xlsx");
            }

        }

        private byte[] CreateExcelStockIssueFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_THONGTIN_PHATHANH_CP");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Ngày thông báo";
                worksheet.Cells[1, 3].Value = "Hình thức phát hành";
                worksheet.Cells[1, 4].Value = "Loại CP";
                worksheet.Cells[1, 5].Value = "Tỷ lệ PP, thực hiện";
                worksheet.Cells[1, 6].Value = "Tình trạng";
                worksheet.Cells[1, 7].Value = "Số CP TT phát hành";
                worksheet.Cells[1, 8].Value = "Số CP dự kiến";
                worksheet.Cells[1, 9].Value = "Giá phát hành";
                worksheet.Cells[1, 10].Value = "Giá trị";
                worksheet.Cells[1, 11].Value = "Ngày ĐK cuối";
                worksheet.Cells[1, 12].Value = "Ngày CT phát hành";
                worksheet.Cells[1, 13].Value = "Năm phát hành";
                worksheet.Cells[1, 14].Value = "Ngày GD không HQ";
                worksheet.Cells[1, 15].Value = "Ngày UBCK chấp nhận";
                worksheet.Cells[1, 16].Value = "Được phép chuyển nhượng";
                worksheet.Cells[1, 17].Value = "Mục đích";
                worksheet.Cells[1, 18].Value = "Ngày chính thức NY";
                worksheet.Cells[1, 19].Value = "Ngày chính thức GD";

                using (var range = worksheet.Cells[1, 1, 1, 19])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.AnDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.IssueMethod;
                    worksheet.Cells[row, 4].Value = item.ShareType;
                    worksheet.Cells[row, 5].Value = item.IssueRate;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 6].Value = item.Status;
                    worksheet.Cells[row, 7].Value = item.OfficialQuantity;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 8].Value = item.Quantity;
                    worksheet.Cells[row, 8].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 9].Value = item.ExPrice;
                    worksheet.Cells[row, 9].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 10].Value = item.TotalValue;
                    worksheet.Cells[row, 10].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 11].Value = item.LastRegDate;
                    worksheet.Cells[row, 11].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 12].Value = item.AnOfficialIssueDate;
                    worksheet.Cells[row, 12].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 13].Value = item.YearFicicalTrade;
                    worksheet.Cells[row, 14].Value = item.ExDateT3;
                    worksheet.Cells[row, 14].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 15].Value = item.UBCKAcceptDate;
                    worksheet.Cells[row, 15].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 16].Value = item.TranferAble;
                    worksheet.Cells[row, 17].Value = item.Goal;

                    worksheet.Cells[row, 18].Value = item.ListedAvailiableDate;
                    worksheet.Cells[row, 18].Style.Numberformat.Format = "dd/mm/yyyy";

                    worksheet.Cells[row, 19].Value = item.OfficialTradeDate;
                    worksheet.Cells[row, 19].Style.Numberformat.Format = "dd/mm/yyyy";

                    worksheet.Cells[row, 11].Value = RPTUtils.RemoveInvalidXmlChars(item.DealMethod);

                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 19])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 19; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}