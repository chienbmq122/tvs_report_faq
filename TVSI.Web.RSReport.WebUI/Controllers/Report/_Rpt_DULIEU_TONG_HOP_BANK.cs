﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_DU_LIEU_TONG_HOP_BANK()
        {
            return View();
        }

        public FileContentResult ExportAllDataBank(string lengthReport, string yearReport)
        {


            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_DU_LIEU_TONG_HOP_BANK",
                     new { iLengthreport = lengthReport, iyearreport = yearReport },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelAllDataBankFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_DULIEU_TONG_HOP_BANK.xlsx");
            }

        }

        private byte[] CreateExcelAllDataBankFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_RPT_DULIEU_TONG_HOP_BANK");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Sàn";
                worksheet.Cells[1, 3].Value = "Tiền mặt, vàng bạc, đá quý";
                worksheet.Cells[1, 4].Value = "Tiền gửi tại ngân hàng nhà nước Việt Nam";
                worksheet.Cells[1, 5].Value = "Tiền gửi tại các TCTD khác và cho vay các TCTD khác";
                worksheet.Cells[1, 6].Value = "Chứng khoán kinh doanh";
                worksheet.Cells[1, 7].Value = "Chứng khoán kinh doanh";
                worksheet.Cells[1, 8].Value = "Dự phòng giảm giá chứng khoán kinh doanh";
                worksheet.Cells[1, 9].Value = "Các công cụ tài chính phái sinh và các tài sản tài chính khác";
                worksheet.Cells[1, 10].Value = "Cho vay khách hàng";
                worksheet.Cells[1, 11].Value = "Cho vay khách hàng";
                worksheet.Cells[1, 12].Value = "Dự phòng rủi ro cho vay khách hàng";
                worksheet.Cells[1, 13].Value = "Chứng khoán đầu tư";
                worksheet.Cells[1, 14].Value = "Chứng khoán đầu tư sẵn sàng để bán";
                worksheet.Cells[1, 15].Value = "Chứng khoán đầu tư giữ đến ngày đáo hạn";
                worksheet.Cells[1, 16].Value = "Dự phòng giảm giá chứng khoán đầu tư";
                worksheet.Cells[1, 17].Value = "Góp vốn, đầu tư dài hạn";
                worksheet.Cells[1, 18].Value = "Đầu tư vào công ty con";
                worksheet.Cells[1, 19].Value = "Đầu tư vào công ty liên doanh";
                worksheet.Cells[1, 20].Value = "Đầu tư dài hạn khác";
                worksheet.Cells[1, 21].Value = "Dự phòng giảm giá đầu tư dài hạn";
                worksheet.Cells[1, 22].Value = "Tài sản cố định";
                worksheet.Cells[1, 23].Value = "Tài sản cố định hữu hình";
                worksheet.Cells[1, 24].Value = "Tài sản cố định thuê tài chính";
                worksheet.Cells[1, 25].Value = "Tài sản cố định vô hình";
                worksheet.Cells[1, 26].Value = "Bất động sản đầu tư";
                worksheet.Cells[1, 27].Value = "Tài sản Có khác";
                worksheet.Cells[1, 28].Value = "Tổng Tài sản";
                worksheet.Cells[1, 29].Value = "Tổng nợ phải trả";
                worksheet.Cells[1, 30].Value = "Các khoản nợ chính phủ và NHNN Việt Nam";
                worksheet.Cells[1, 31].Value = "Tiền gửi và vay các Tổ chức tín dụng khác";
                worksheet.Cells[1, 32].Value = "Tiền gửi của khách hàng";
                worksheet.Cells[1, 33].Value = "Các công cụ tài chính phái sinh và các khoản nợ tài chính khác";
                worksheet.Cells[1, 34].Value = "Vốn tài trợ, uỷ thác đầu tư của Chính phủ và các tổ chức tín";
                worksheet.Cells[1, 35].Value = "Phát hành giấy tờ có giá";
                worksheet.Cells[1, 36].Value = "Các khoản nợ khác";
                worksheet.Cells[1, 37].Value = "Vốn chủ sở hữu";
                worksheet.Cells[1, 38].Value = "Vốn của tổ chức tín dụng";
                worksheet.Cells[1, 39].Value = "Vốn điều lệ";
                worksheet.Cells[1, 40].Value = "Vốn đầu tư XDCB";
                worksheet.Cells[1, 41].Value = "Thặng dư vốn CP";
                worksheet.Cells[1, 42].Value = "Cổ phiếu Quỹ";
                worksheet.Cells[1, 43].Value = "Cổ phiếu ưu đãi";
                worksheet.Cells[1, 44].Value = "Vốn khác";
                worksheet.Cells[1, 45].Value = "Quỹ của tổ chức tín dụng";
                worksheet.Cells[1, 46].Value = "Chênh lệch tỷ giá hối đoái";
                worksheet.Cells[1, 47].Value = "Chênh lệch đánh giá lại tài sản";
                worksheet.Cells[1, 48].Value = "Lợi nhuận chưa phân phối";
                worksheet.Cells[1, 49].Value = "Lợi ích của cổ đông thiểu số";
                worksheet.Cells[1, 50].Value = "Nợ phải trả và vốn chủ sở hữu";
                worksheet.Cells[1, 51].Value = "Thu nhập lãi và các khoản thu nhập tương tự";
                worksheet.Cells[1, 52].Value = "Chi phí lãi và các chi phí tương tự";
                worksheet.Cells[1, 53].Value = "Thu nhập lãi thuần";
                worksheet.Cells[1, 54].Value = "Thu nhập từ hoạt động dịch vụ";
                worksheet.Cells[1, 55].Value = "chi phí hoạt động dịch vụ";
                worksheet.Cells[1, 56].Value = "Lãi thuần từ hoạt động dịch vụ";
                worksheet.Cells[1, 57].Value = "Lãi thuần từ hoạy động kinh doanh ngoại hối và vàng";
                worksheet.Cells[1, 58].Value = "Lãi thuần từ mua bán chứng khoán kinh doanh";
                worksheet.Cells[1, 59].Value = "Lãi thuần từ mua bán chứng khoán đầu tư";
                worksheet.Cells[1, 60].Value = "Thu nhập từ hoạt động khác";
                worksheet.Cells[1, 61].Value = "Chi phí hoạt động khác";
                worksheet.Cells[1, 62].Value = "Lãi thuần từ hoạt động khác";
                worksheet.Cells[1, 63].Value = "Thu nhập từ góp vốn, mua cổ phần";
                worksheet.Cells[1, 64].Value = "Tổng thu nhập hoạt động";
                worksheet.Cells[1, 65].Value = "Chi phí hoạt động";
                worksheet.Cells[1, 66].Value = "LN thuần từ hoạt động kinh doanh trước CF dự phòng rủi ro tín dụng";
                worksheet.Cells[1, 67].Value = "Chi phí dự phòng rủi ro tín dụng";
                worksheet.Cells[1, 68].Value = "Tổng lợi nhuận trước thuế";
                worksheet.Cells[1, 69].Value = "Chi phí thuế TNDN hiện hành";
                worksheet.Cells[1, 70].Value = "Chi phí thuế TNDN hoãn lại";
                worksheet.Cells[1, 71].Value = "Lợi nhuận sau thuế";
                worksheet.Cells[1, 72].Value = "Lợi ích của cổ đông thiểu số";
                worksheet.Cells[1, 73].Value = "Lãi cơ bản trên cổ phiếu";
                worksheet.Cells[1, 74].Value = "1 - truc tiep, 0 - Gian tiep";
                worksheet.Cells[1, 75].Value = "Thu nhập lãi và các khoản tương đương";
                worksheet.Cells[1, 76].Value = "Chi phí lãi và các khoản tương đương";
                worksheet.Cells[1, 77].Value = "Thu nhập từ hoạt động dịch vụ nhận được";
                worksheet.Cells[1, 78].Value = "Thu nhập thuần từ hoạt động kinh doanh ngoại hối và vàng";
                worksheet.Cells[1, 79].Value = "Thu nhập từ hoạt động kinh doanh chứng khoán";
                worksheet.Cells[1, 80].Value = "Thu nhập khác";
                worksheet.Cells[1, 81].Value = "Thanh toán cho nhân viên và nhà cung cấp";
                worksheet.Cells[1, 82].Value = "Thuế thu nhập doanh nghiệp trong năm";
                worksheet.Cells[1, 83].Value = "Lợi nhuận trước thuế";
                worksheet.Cells[1, 84].Value = "Khấu hao TSCĐ, bất động sản đầu tư";
                worksheet.Cells[1, 85].Value = "dự phòng RR tín dụng, giảm giá, đầu tư tăng thêm/(hoàn nhập) trong năm";
                worksheet.Cells[1, 86].Value = "Lãi và phí phải thu trong kỳ (thực tế chưa thu)";
                worksheet.Cells[1, 87].Value = "Lãi và ohí phải trả trong kỳ (thực tế chưa trả)";
                worksheet.Cells[1, 88].Value = "Lãi/lỗ do thanh lý tài sản cố định";
                worksheet.Cells[1, 89].Value = "Lãi/lỗ do bán/thanh lý bất động sản đầu tư";
                worksheet.Cells[1, 90].Value = "Lãi/lỗ do thanh lý những khoản ĐT, Góp,lãi, cổ tức, lợi nhuận được chia";
                worksheet.Cells[1, 91].Value = "Chênh lệch tỷ giá hối đoái chưa thực hiện";
                worksheet.Cells[1, 92].Value = "Các điều chỉnh khác";
                worksheet.Cells[1, 93].Value = "Lưu chuyển tiền thuần từ HĐKD trước những thay đổi về TS và CN";
                worksheet.Cells[1, 94].Value = "Tiền gửi tại NHNN";
                worksheet.Cells[1, 95].Value = "Tăng/Giảm các khoản tiền gửi và cho vay các tổ chức tín dụng khác";
                worksheet.Cells[1, 96].Value = "Tăng/giảm các khoản về kinh doanh chứng khoán";
                worksheet.Cells[1, 97].Value = "Tăng/Giảm các công cụ tài chính phái sinh và các tài sản tài chính khác";
                worksheet.Cells[1, 98].Value = "Tăng/Giảm các khoản cho vay khách hàng";
                worksheet.Cells[1, 99].Value = "Tăng/Giảm lãi, phí phải thu";
                worksheet.Cells[1, 100].Value = "Tăng/Giảm nguồn dự phòng để bù đắp tổn thất các khoản ";
                worksheet.Cells[1, 101].Value = "Tăng/Giảm khác về tài sản hoạt động";
                worksheet.Cells[1, 102].Value = "Tăng/Giảm các khoản nợ chính phủ và NHNN";
                worksheet.Cells[1, 103].Value = "Tăng/Giảm các khoản tiền gửi và vay các TCTD khác";
                worksheet.Cells[1, 104].Value = "Tăng/Giảm tiền gửi của khách hàng";
                worksheet.Cells[1, 105].Value = "Tăng/Giảm các công cụ tài chính phái sinh và các khoản nợ tài chính khác";
                worksheet.Cells[1, 106].Value = "Tăng/Giảm vốn tài trợ, uỷ thác đầu tư của chính phủ và các TCTD khác";
                worksheet.Cells[1, 107].Value = "Tăng/Giảm phát hành giấy tờ có giá";
                worksheet.Cells[1, 108].Value = "Tăng/Giảm lãi, phí phải trả";
                worksheet.Cells[1, 109].Value = "Tăng/Giảm khác về công nợ hoạt động";
                worksheet.Cells[1, 110].Value = "Lưu chuyển tiền thuần từ hoạt động kinh doanh trước thuế thu nhập DN";
                worksheet.Cells[1, 111].Value = "Thuế TNDN đã nộp";
                worksheet.Cells[1, 112].Value = "Chi từ các quỹ của TCTD";
                worksheet.Cells[1, 113].Value = "Thu được từ nợ khó đòi";
                worksheet.Cells[1, 114].Value = "Lưu chuyển tiền thuần từ hoạt động kinh doanh";
                worksheet.Cells[1, 115].Value = "Mua sắm TSCĐ";
                worksheet.Cells[1, 116].Value = "Tiền thu từ thanh lý, nhượng bán TSCĐ";
                worksheet.Cells[1, 117].Value = "Tiền chi từ thanh lý, nhượng bán TSCĐ";
                worksheet.Cells[1, 118].Value = "Mua sắm Bất động sản đầu tư";
                worksheet.Cells[1, 119].Value = "Tiền thu từ bán, thanh lý bất động sản đầu tư";
                worksheet.Cells[1, 120].Value = "Tiền chi ra do bán, thanh lý bất động sản đầu tư";
                worksheet.Cells[1, 121].Value = "Tiền chi đầu tư, góp vốn vào các đơn vị khác";
                worksheet.Cells[1, 122].Value = "Tiền thu từ đầu tư, góp vốn vào các đơn vị khác";
                worksheet.Cells[1, 123].Value = "Tiền thu cổ tức và lợi nhuận được chia ";
                worksheet.Cells[1, 124].Value = "Lưu chuyển từ hoạt động đầu tư";
                worksheet.Cells[1, 125].Value = "Tăng vốn cổ phần từ góp vốn và/hoặc phát hành cổ phiếu";
                worksheet.Cells[1, 126].Value = "Tiền thu từ PHGTCS dài hạn đủ điều kiện tính vào vốn tự có…";
                worksheet.Cells[1, 127].Value = "Tiền chi thanh toán GTCSDH đủ điều kiện tính vào vốn tự có …";
                worksheet.Cells[1, 128].Value = "Cổ tức trả cho cổ đông, lợi nhuận đã chia";
                worksheet.Cells[1, 129].Value = "Tiền chi ra mua cổ phiếu quỹ";
                worksheet.Cells[1, 130].Value = "Tiền thu được do bán cổ phiếu quỹ";
                worksheet.Cells[1, 131].Value = "Lưu chuyển tiền từ hoạt động tài chính";
                worksheet.Cells[1, 132].Value = "Lưu chuyển tiền thuần trong kỳ";
                worksheet.Cells[1, 133].Value = "Tiền và các khoản tương đương tiền tài thời điểm đầu kỳ";
                worksheet.Cells[1, 134].Value = "Điều chỉnh ảnh hưởng của thay đổi tỷ giá";
                worksheet.Cells[1, 135].Value = "Tiền và các khoản tương đương tiền tại thời điểm cuối kỳ";
                worksheet.Cells[1, 136].Value = "Thương mại";
                worksheet.Cells[1, 137].Value = "Nông nghiệp và lâm nghiệp";
                worksheet.Cells[1, 138].Value = "Sản xuất";
                worksheet.Cells[1, 139].Value = "Xây dựng";
                worksheet.Cells[1, 140].Value = "Dịch vụ cộng đồng và cá nhân";
                worksheet.Cells[1, 141].Value = "Kho bãi,vận tải, viễn thông";
                worksheet.Cells[1, 142].Value = "Giáo dục và đào tạo";
                worksheet.Cells[1, 143].Value = "bất động sản và tư vấn";
                worksheet.Cells[1, 144].Value = "Khách sạn và nhà hàng";
                worksheet.Cells[1, 145].Value = "Dịch vụ tài chính";
                worksheet.Cells[1, 146].Value = "Các ngành khác";
                worksheet.Cells[1, 147].Value = "Nợ đủ tiêu chuẩn";
                worksheet.Cells[1, 148].Value = "Nợ cần chú ý";
                worksheet.Cells[1, 149].Value = "Nợ dưới tiêu chuẩn";
                worksheet.Cells[1, 150].Value = "Nợ nghi ngờ";
                worksheet.Cells[1, 151].Value = "Nợ xấu có khả năng mất vốn";
                worksheet.Cells[1, 152].Value = "Cho vay các tổ chức kinh tế, cá nhân trong nước";
                worksheet.Cells[1, 153].Value = "Chiết khấu thương phiếu và giấy tờ có giá";
                worksheet.Cells[1, 154].Value = "Cho thuê tài chính";
                worksheet.Cells[1, 155].Value = "Các khoản trả thay khách hàng";
                worksheet.Cells[1, 156].Value = "Cho vay bằng vốn tài trợ, uỷ thác đầu tư";
                worksheet.Cells[1, 157].Value = "Cho vay đối với các tổ chức, cá nhân nước ngoài";
                worksheet.Cells[1, 158].Value = "Cho vay theo chỉ định của Chính phủ";
                worksheet.Cells[1, 159].Value = "Nợ cho vay được khoanh và nợ chờ xử lý";
                worksheet.Cells[1, 160].Value = "Các khoản cho vay khác";
                worksheet.Cells[1, 161].Value = "Cho vay ngắn hạn";
                worksheet.Cells[1, 162].Value = "Cho vay trung hạn";
                worksheet.Cells[1, 163].Value = "Cho vay  dài hạn";
                worksheet.Cells[1, 164].Value = "VNĐ";
                worksheet.Cells[1, 165].Value = "Ngoại tệ và vàng";
                worksheet.Cells[1, 166].Value = "TP Hồ Chí Minh";
                worksheet.Cells[1, 167].Value = "Hà Nội";
                worksheet.Cells[1, 168].Value = "Đồng bằng sông Cửu Long";
                worksheet.Cells[1, 169].Value = "Miền trung";
                worksheet.Cells[1, 170].Value = "Vùng Khác";
                worksheet.Cells[1, 171].Value = "Tiền gửi không kỳ hạn";
                worksheet.Cells[1, 172].Value = "Tiền gửi có kỳ hạn";
                worksheet.Cells[1, 173].Value = "Tiền gửi tiết kiệm";
                worksheet.Cells[1, 174].Value = "tiền gửi ký quỹ";
                worksheet.Cells[1, 175].Value = "Tiền gửi cho những mục đích riêng biệt";
                worksheet.Cells[1, 176].Value = "VNĐ";
                worksheet.Cells[1, 177].Value = "Ngoại tệ";
                worksheet.Cells[1, 178].Value = "Doanh nghiệp nhà nước";
                worksheet.Cells[1, 179].Value = "Doanh nghiệp tư nhân";
                worksheet.Cells[1, 180].Value = "Doanh nghiệp nước ngoài";
                worksheet.Cells[1, 181].Value = "Cá nhân";
                worksheet.Cells[1, 182].Value = "Đối tượng khác";


                using (var range = worksheet.Cells[1, 1, 1, 182])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.San;
                    worksheet.Cells[row, 3].Value = item.F3_2;
                    worksheet.Cells[row, 4].Value = item.F3_3;
                    worksheet.Cells[row, 5].Value = item.F3_4;
                    worksheet.Cells[row, 6].Value = item.F3_5;
                    worksheet.Cells[row, 7].Value = item.F3_6;
                    worksheet.Cells[row, 8].Value = item.F3_7;
                    worksheet.Cells[row, 9].Value = item.F3_8;
                    worksheet.Cells[row, 10].Value = item.F3_9;
                    worksheet.Cells[row, 11].Value = item.F3_10;
                    worksheet.Cells[row, 12].Value = item.F3_11;
                    worksheet.Cells[row, 13].Value = item.F3_12;
                    worksheet.Cells[row, 14].Value = item.F3_13;
                    worksheet.Cells[row, 15].Value = item.F3_14;
                    worksheet.Cells[row, 16].Value = item.F3_15;
                    worksheet.Cells[row, 17].Value = item.F3_16;
                    worksheet.Cells[row, 18].Value = item.F3_17;
                    worksheet.Cells[row, 19].Value = item.F3_18;
                    worksheet.Cells[row, 20].Value = item.F3_19;
                    worksheet.Cells[row, 21].Value = item.F3_20;
                    worksheet.Cells[row, 22].Value = item.F3_21;
                    worksheet.Cells[row, 23].Value = item.F3_22;
                    worksheet.Cells[row, 24].Value = item.F3_23;
                    worksheet.Cells[row, 25].Value = item.F3_24;
                    worksheet.Cells[row, 26].Value = item.F3_25;
                    worksheet.Cells[row, 27].Value = item.F3_26;
                    worksheet.Cells[row, 28].Value = item.F3_1;
                    worksheet.Cells[row, 29].Value = item.F3_28;
                    worksheet.Cells[row, 30].Value = item.F3_29;
                    worksheet.Cells[row, 31].Value = item.F3_30;
                    worksheet.Cells[row, 32].Value = item.F3_31;
                    worksheet.Cells[row, 33].Value = item.F3_32;
                    worksheet.Cells[row, 34].Value = item.F3_33;
                    worksheet.Cells[row, 35].Value = item.F3_34;
                    worksheet.Cells[row, 36].Value = item.F3_35;
                    worksheet.Cells[row, 37].Value = item.F3_36;
                    worksheet.Cells[row, 38].Value = item.F3_37;
                    worksheet.Cells[row, 39].Value = item.F3_37a;
                    worksheet.Cells[row, 40].Value = item.F3_37b;
                    worksheet.Cells[row, 41].Value = item.F3_37c;
                    worksheet.Cells[row, 42].Value = item.F3_37d;
                    worksheet.Cells[row, 43].Value = item.F3_37e;
                    worksheet.Cells[row, 44].Value = item.F3_37f;
                    worksheet.Cells[row, 45].Value = item.F3_38;
                    worksheet.Cells[row, 46].Value = item.F3_39;
                    worksheet.Cells[row, 47].Value = item.F3_40;
                    worksheet.Cells[row, 48].Value = item.F3_41;
                    worksheet.Cells[row, 49].Value = item.F3_42;
                    worksheet.Cells[row, 50].Value = item.F3_27;
                    worksheet.Cells[row, 51].Value = item.F3_43;
                    worksheet.Cells[row, 52].Value = item.F3_44;
                    worksheet.Cells[row, 53].Value = item.F3_45;
                    worksheet.Cells[row, 54].Value = item.F3_46;
                    worksheet.Cells[row, 55].Value = item.F3_47;
                    worksheet.Cells[row, 56].Value = item.F3_48;
                    worksheet.Cells[row, 57].Value = item.F3_49;
                    worksheet.Cells[row, 58].Value = item.F3_50;
                    worksheet.Cells[row, 59].Value = item.F3_51;
                    worksheet.Cells[row, 60].Value = item.F3_52;
                    worksheet.Cells[row, 61].Value = item.F3_53;
                    worksheet.Cells[row, 62].Value = item.F3_54;
                    worksheet.Cells[row, 63].Value = item.F3_55;
                    worksheet.Cells[row, 64].Value = item.F3_55B;
                    worksheet.Cells[row, 65].Value = item.F3_56;
                    worksheet.Cells[row, 66].Value = item.F3_57;
                    worksheet.Cells[row, 67].Value = item.F3_58;
                    worksheet.Cells[row, 68].Value = item.F3_59;
                    worksheet.Cells[row, 69].Value = item.F3_60;
                    worksheet.Cells[row, 70].Value = item.F3_61;
                    worksheet.Cells[row, 71].Value = item.F3_63;
                    worksheet.Cells[row, 72].Value = item.F3_64;
                    worksheet.Cells[row, 73].Value = item.F3_65;
                    worksheet.Cells[row, 74].Value = item.TT_GT;
                    worksheet.Cells[row, 75].Value = item.F3_180;
                    worksheet.Cells[row, 76].Value = item.F3_181;
                    worksheet.Cells[row, 77].Value = item.F3_182;
                    worksheet.Cells[row, 78].Value = item.F3_183;
                    worksheet.Cells[row, 79].Value = item.F3_184;
                    worksheet.Cells[row, 80].Value = item.F3_185;
                    worksheet.Cells[row, 81].Value = item.F3_186;
                    worksheet.Cells[row, 82].Value = item.F3_187;
                    worksheet.Cells[row, 83].Value = item.F3_66;
                    worksheet.Cells[row, 84].Value = item.F3_67;
                    worksheet.Cells[row, 85].Value = item.F3_68;
                    worksheet.Cells[row, 86].Value = item.F3_69;
                    worksheet.Cells[row, 87].Value = item.F3_70;
                    worksheet.Cells[row, 88].Value = item.F3_71;
                    worksheet.Cells[row, 89].Value = item.F3_72;
                    worksheet.Cells[row, 90].Value = item.F3_73;
                    worksheet.Cells[row, 91].Value = item.F3_74;
                    worksheet.Cells[row, 92].Value = item.F3_75;
                    worksheet.Cells[row, 93].Value = item.F3_76;
                    worksheet.Cells[row, 94].Value = item.F3_77;
                    worksheet.Cells[row, 95].Value = item.F3_78;
                    worksheet.Cells[row, 96].Value = item.F3_79;
                    worksheet.Cells[row, 97].Value = item.F3_80;
                    worksheet.Cells[row, 98].Value = item.F3_81;
                    worksheet.Cells[row, 99].Value = item.F3_82;
                    worksheet.Cells[row, 100].Value = item.F3_83;
                    worksheet.Cells[row, 101].Value = item.F3_84;
                    worksheet.Cells[row, 102].Value = item.F3_85;
                    worksheet.Cells[row, 103].Value = item.F3_86;
                    worksheet.Cells[row, 104].Value = item.F3_87;
                    worksheet.Cells[row, 105].Value = item.F3_88;
                    worksheet.Cells[row, 106].Value = item.F3_89;
                    worksheet.Cells[row, 107].Value = item.F3_90;
                    worksheet.Cells[row, 108].Value = item.F3_91;
                    worksheet.Cells[row, 109].Value = item.F3_92;
                    worksheet.Cells[row, 110].Value = item.F3_93;
                    worksheet.Cells[row, 111].Value = item.F3_94;
                    worksheet.Cells[row, 112].Value = item.F3_95;
                    worksheet.Cells[row, 113].Value = item.F3_96;
                    worksheet.Cells[row, 114].Value = item.F3_97;
                    worksheet.Cells[row, 115].Value = item.F3_98;
                    worksheet.Cells[row, 116].Value = item.F3_99;
                    worksheet.Cells[row, 117].Value = item.F3_100;
                    worksheet.Cells[row, 118].Value = item.F3_101;
                    worksheet.Cells[row, 119].Value = item.F3_102;
                    worksheet.Cells[row, 120].Value = item.F3_103;
                    worksheet.Cells[row, 121].Value = item.F3_104;
                    worksheet.Cells[row, 122].Value = item.F3_105;
                    worksheet.Cells[row, 123].Value = item.F3_106;
                    worksheet.Cells[row, 124].Value = item.F3_107;
                    worksheet.Cells[row, 125].Value = item.F3_108;
                    worksheet.Cells[row, 126].Value = item.F3_109;
                    worksheet.Cells[row, 127].Value = item.F3_110;
                    worksheet.Cells[row, 128].Value = item.F3_111;
                    worksheet.Cells[row, 129].Value = item.F3_112;
                    worksheet.Cells[row, 130].Value = item.F3_113;
                    worksheet.Cells[row, 131].Value = item.F3_114;
                    worksheet.Cells[row, 132].Value = item.F3_115;
                    worksheet.Cells[row, 133].Value = item.F3_116;
                    worksheet.Cells[row, 134].Value = item.F3_117;
                    worksheet.Cells[row, 135].Value = item.F3_118;
                    worksheet.Cells[row, 136].Value = item.F3_132;
                    worksheet.Cells[row, 137].Value = item.F3_133;
                    worksheet.Cells[row, 138].Value = item.F3_134;
                    worksheet.Cells[row, 139].Value = item.F3_135;
                    worksheet.Cells[row, 140].Value = item.F3_136;
                    worksheet.Cells[row, 141].Value = item.F3_137;
                    worksheet.Cells[row, 142].Value = item.F3_138;
                    worksheet.Cells[row, 143].Value = item.F3_139;
                    worksheet.Cells[row, 144].Value = item.F3_140;
                    worksheet.Cells[row, 145].Value = item.F3_141;
                    worksheet.Cells[row, 146].Value = item.F3_142;
                    worksheet.Cells[row, 147].Value = item.F3_144;
                    worksheet.Cells[row, 148].Value = item.F3_145;
                    worksheet.Cells[row, 149].Value = item.F3_146;
                    worksheet.Cells[row, 150].Value = item.F3_147;
                    worksheet.Cells[row, 151].Value = item.F3_148;
                    worksheet.Cells[row, 152].Value = item.F3_122;
                    worksheet.Cells[row, 153].Value = item.F3_123;
                    worksheet.Cells[row, 154].Value = item.F3_124;
                    worksheet.Cells[row, 155].Value = item.F3_125;
                    worksheet.Cells[row, 156].Value = item.F3_126;
                    worksheet.Cells[row, 157].Value = item.F3_127;
                    worksheet.Cells[row, 158].Value = item.F3_128;
                    worksheet.Cells[row, 159].Value = item.F3_129;
                    worksheet.Cells[row, 160].Value = item.F3_130;
                    worksheet.Cells[row, 161].Value = item.F3_150;
                    worksheet.Cells[row, 162].Value = item.F3_151;
                    worksheet.Cells[row, 163].Value = item.F3_152;
                    worksheet.Cells[row, 164].Value = item.F3_154;
                    worksheet.Cells[row, 165].Value = item.F3_155;
                    worksheet.Cells[row, 166].Value = item.F3_157;
                    worksheet.Cells[row, 167].Value = item.F3_158;
                    worksheet.Cells[row, 168].Value = item.F3_159;
                    worksheet.Cells[row, 169].Value = item.F3_160;
                    worksheet.Cells[row, 170].Value = item.F3_161;
                    worksheet.Cells[row, 171].Value = item.F3_164;
                    worksheet.Cells[row, 172].Value = item.F3_165;
                    worksheet.Cells[row, 173].Value = item.F3_166;
                    worksheet.Cells[row, 174].Value = item.F3_167;
                    worksheet.Cells[row, 175].Value = item.F3_168;
                    worksheet.Cells[row, 176].Value = item.F3_170;
                    worksheet.Cells[row, 177].Value = item.F3_171;
                    worksheet.Cells[row, 178].Value = item.F3_173;
                    worksheet.Cells[row, 179].Value = item.F3_174;
                    worksheet.Cells[row, 180].Value = item.F3_175;
                    worksheet.Cells[row, 181].Value = item.F3_176;
                    worksheet.Cells[row, 182].Value = item.F3_177;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 182])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 182; i++)
                {
                    for (int j = 1; j <= list.Count() + 1; j++)
                    {
                        if (i >= 2)
                            worksheet.Cells[j, i].Style.Numberformat.Format = "#,###";
                    }

                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}