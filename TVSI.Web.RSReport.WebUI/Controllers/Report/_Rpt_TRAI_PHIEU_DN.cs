﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Utility;
namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_TRAI_PHIEU_DN()
        {
            return View();
        }

        public FileContentResult ExportDebts(string Code, string fromDate, string toDate)
        {

            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_TRAIPHIEU_DN",
                    new { Code, Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelDebtsFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_TRAIPHIEU_DN.xlsx");
            }

        }

        private byte[] CreateExcelDebtsFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_TRAIPHIEU_DN");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Ngày thông báo";
                worksheet.Cells[1, 3].Value = "Mã trái phiểu chuyển đổi";
                worksheet.Cells[1, 4].Value = "Số công văn";
                worksheet.Cells[1, 5].Value = "Ngày GD không hưởng quyền";
                worksheet.Cells[1, 6].Value = "Ngày phát hành";
                worksheet.Cells[1, 7].Value = "KL dự kiến phát hành";
                worksheet.Cells[1, 8].Value = "KL thực tế phát hành";
                worksheet.Cells[1, 9].Value = "Mệnh giá trái phiếu (VND)";
                worksheet.Cells[1, 10].Value = "Tổng mệnh giá (tỷ VND)";
                worksheet.Cells[1, 11].Value = "Tình trạng";
                worksheet.Cells[1, 12].Value = "Lãi suất Coupon";
                worksheet.Cells[1, 13].Value = "Định kỳ trả lãi";
                worksheet.Cells[1, 14].Value = "Giá phát hành";
                worksheet.Cells[1, 15].Value = "Kỳ hạn TP";
                worksheet.Cells[1, 16].Value = "Tỷ lệ chuyển đổi thành CP";
                worksheet.Cells[1, 17].Value = "Thời hạn chuyển đổi";
                worksheet.Cells[1, 18].Value = "Đối tượng phát hành";
                worksheet.Cells[1, 19].Value = "Mục đích";
                worksheet.Cells[1, 20].Value = "Thông tin thêm";
                worksheet.Cells[1, 21].Value = "Đơn vị BLPH";
                worksheet.Cells[1, 22].Value = "Đơn vị TVPH";
                worksheet.Cells[1, 23].Value = "Ghi chú";
                worksheet.Cells[1, 24].Value = "Loại trái phiếu";
                using (var range = worksheet.Cells[1, 1, 1, 24])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.AnDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.NoDebts;
                    worksheet.Cells[row, 4].Value = item.NoDispatch;
                    worksheet.Cells[row, 5].Value = item.ExDate;
                    worksheet.Cells[row, 5].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 6].Value = item.IssueDate;
                    worksheet.Cells[row, 6].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 7].Value = item.ReQuantity;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 8].Value = item.RealQuantity;
                    worksheet.Cells[row, 8].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 9].Value = item.ParValue;
                    worksheet.Cells[row, 9].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 10].Value = item.TotalValue;
                    worksheet.Cells[row, 10].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 11].Value = item.Status;
                    worksheet.Cells[row, 12].Value = item.Coupon;
                    worksheet.Cells[row, 12].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 13].Value = item.PostPay;
                    worksheet.Cells[row, 14].Value = item.IssuePrice;
                    worksheet.Cells[row, 14].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 15].Value = item.MaturialDate;
                    worksheet.Cells[row, 15].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 16].Value = item.ConversionRatio;
                    worksheet.Cells[row, 16].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 17].Value = item.TimeChangeLimit;
                    worksheet.Cells[row, 18].Value = item.Object;
                    worksheet.Cells[row, 19].Value = item.Goal;
                    worksheet.Cells[row, 20].Value = item.AddInfo;
                    worksheet.Cells[row, 21].Value = item.BailCorp;
                    worksheet.Cells[row, 22].Value = item.AdvisoryCorp;
                    worksheet.Cells[row, 23].Value = item.Note;
                    worksheet.Cells[row, 24].Value = item.Debts;
                    row++;
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 24])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 24; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}