﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TVSI.Web.RSReport.Lib.Models;
using TVSI.Web.RSReport.Lib.Utility;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {

        public ActionResult Rpt_TYLE_COTUC()
        {
            return View();
        }

        public FileContentResult ExportTyleCotuc(string code, string fromDate, string toDate)
        {
            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query<TyleCotucViewModel>("TVSI_sRS_TYLECOTUC",
                    new {code, Startdate = fromDate, Enddate = toDate },
                        commandType: CommandType.StoredProcedure).ToList();

                var fileContent = CreateExcelFileTyleCotuc(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_TYLECOTUC.xlsx");
            }

        }

        private byte[] CreateExcelFileTyleCotuc(List<TyleCotucViewModel> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_TYLECOTUC");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Tỷ lệ trả cổ tức";
                worksheet.Cells[1, 3].Value = "Giá trị cổ tức";
                worksheet.Cells[1, 4].Value = "Năm";
                worksheet.Cells[1, 5].Value = "Ghi chú";


                using (var range = worksheet.Cells[1, 1, 1, 5])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    


                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.Tyletracotuc;
                    worksheet.Cells[row, 3].Value = item.Cotuccuanam;
                    worksheet.Cells[row, 3].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 4].Value = item.YearReport;
                    worksheet.Cells[row, 5].Value = RPTUtils.RemoveInvalidXmlChars(item.Note);
                    //worksheet.Cells[row, 5].Style.Numberformat.Format = "@";
                    row++;
                }

                if (list.Count > 0)
                {
                    using (var range = worksheet.Cells[2, 1, list.Count + 1, 5])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 5; i++)
                {
                    worksheet.Column(i).AutoFit();
                }
                //Response.ContentEncoding = System.Text.Encoding.UTF8;
                //Response.Charset = "utf-8";
                result = package.GetAsByteArray();
              
            }

            return result;
        }
    }
}