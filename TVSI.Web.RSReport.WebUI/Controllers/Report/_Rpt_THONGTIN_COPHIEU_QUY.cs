﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace TVSI.Web.RSReport.WebUI.Controllers
{
    public partial class ReportController
    {
        public ActionResult Rpt_THONGTIN_COPHIEU_QUY()
        {
            return View();
        }

        public FileContentResult ExportTtCophieuQuy(string Code, string fromDate, string toDate)
        {

            var dtxFrom = DateTime.ParseExact(fromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            var dtxTo = DateTime.ParseExact(toDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); 

            using (var conn = new SqlConnection(StoxConnectionStr))
            {
                conn.Open();
                var data = conn.Query("TVSI_sRS_THONGTIN_COPHIEU_QUY",
                    new {Code, Startdate = dtxFrom, Enddate = dtxTo },
                        commandType: CommandType.StoredProcedure);

                var fileContent = CreateExcelTreasurySharesFile(data);
                return File(fileContent, ExcelContentType, "TVSI_RPT_THONGTIN_COPHIEU_QUY.xlsx");
            }

        }

        private byte[] CreateExcelTreasurySharesFile(IEnumerable<dynamic> list)
        {
            byte[] result = null;

            using (ExcelPackage package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("TVSI_BC_THONGTIN_COPHIEU_QUY");
                worksheet.DefaultRowHeight = 16;

                worksheet.Cells[1, 1].Value = "Mã CK";
                worksheet.Cells[1, 2].Value = "Ngày thông báo";
                worksheet.Cells[1, 3].Value = "Số CP đăng ký";
                worksheet.Cells[1, 4].Value = "SL GD thực tế";
                worksheet.Cells[1, 5].Value = "Loại GD";
                worksheet.Cells[1, 6].Value = "Tình trạng GD";
                worksheet.Cells[1, 7].Value = "Ngày bắt đầu";
                worksheet.Cells[1, 8].Value = "Ngày kết thúc";
                worksheet.Cells[1, 9].Value = "CP trước GD";
                worksheet.Cells[1, 10].Value = "CP sau GD";
                worksheet.Cells[1, 11].Value = "Hình thức GD";


                using (var range = worksheet.Cells[1, 1, 1, 11])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.YellowGreen);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    range.Style.ShrinkToFit = false;
                }

                var row = 2;

                foreach (var item in list)
                {
                    worksheet.Cells[row, 1].Value = item.Ticker;
                    worksheet.Cells[row, 2].Value = item.AnDate;
                    worksheet.Cells[row, 2].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 3].Value = item.ReTradeQuantity;
                    worksheet.Cells[row, 3].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 4].Value = item.RealTradeQuantity;
                    worksheet.Cells[row, 4].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 5].Value = item.Action;
                    worksheet.Cells[row, 6].Value = item.Status;
                    worksheet.Cells[row, 7].Value = item.DealStartTime;
                    worksheet.Cells[row, 7].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 8].Value = item.DealStopTime;
                    worksheet.Cells[row, 8].Style.Numberformat.Format = "dd/mm/yyyy";
                    worksheet.Cells[row, 9].Value = item.ShareBeforeTrade;
                    worksheet.Cells[row, 9].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 10].Value = item.ShareAfterTrade;
                    worksheet.Cells[row, 10].Style.Numberformat.Format = "#,###";
                    worksheet.Cells[row, 11].Value = item.DealMethod;

                    row++;			
                }

                if (list.Any())
                {
                    using (var range = worksheet.Cells[2, 1, list.Count() + 1, 11])
                    {
                        range.Style.Font.Bold = false;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }

                for (int i = 1; i <= 11; i++)
                {
                    worksheet.Column(i).AutoFit();
                }

                result = package.GetAsByteArray();
            }

            return result;
        }
    }
}