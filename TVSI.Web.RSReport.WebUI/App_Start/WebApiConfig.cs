﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TVSI.Web.RSReport.Lib.Service;
using Unity;
using Unity.Lifetime;

namespace TVSI.Web.RSReport.WebUI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();

            container.RegisterType<IFAQLevelOneService, FAQLevelOneService>();
            

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
